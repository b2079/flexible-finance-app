import axios, { AxiosInstance } from "axios";
import Constants from "expo-constants";
import { createContext } from "react";

if (Constants.manifest?.extra?.["apiUrl"] === undefined) {
  throw new Error("Could not find API URL");
}
const apiUrl: string = Constants.manifest.extra["apiUrl"];

const publicAxios = axios.create({
  baseURL: apiUrl,
});

const privateAxios = axios.create({
  baseURL: apiUrl,
});

interface AxiosContextInterface {
  apiUrl: string;
  publicAxios: AxiosInstance;
  privateAxios: AxiosInstance;
}

let isSpinnerVisible: any;

function toggleSpinner(value?: any): void {}

const AxiosContext = createContext({
  apiUrl,
  publicAxios,
  privateAxios,
  isSpinnerVisible,
  toggleSpinner,
});

export { AxiosContext, AxiosContextInterface };
