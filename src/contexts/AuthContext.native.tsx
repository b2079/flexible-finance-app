import * as SecureStore from "expo-secure-store";
import { createContext } from "react";

interface AuthState {
  refreshToken: string;
  authenticated: boolean;
  userId: number;
}

async function getAuthInfo(): Promise<string> {
  return (await SecureStore.getItemAsync("authInfo")) ?? "";
}

async function setAuthInfo(
  accessToken: string,
  refreshToken: string,
  userId: number
): Promise<void> {
  return await SecureStore.setItemAsync(
    "authInfo",
    JSON.stringify({ accessToken, refreshToken, userId })
  );
}

async function deleteAuthInfo(): Promise<void> {
  return await SecureStore.deleteItemAsync("authInfo");
}

async function getAccessToken(): Promise<string> {
  const jsonAuthInfo = await getAuthInfo();
  if (jsonAuthInfo !== "") {
    const authInfo = JSON.parse(jsonAuthInfo);
    return authInfo.accessToken as string;
  }
  return "";
}

const authState: AuthState = {
  refreshToken: "",
  authenticated: false,
  userId: NaN,
};

const AuthContext = createContext({
  authState,
  setAuthState: () => {},
  getAuthInfo,
  setAuthInfo,
  deleteAuthInfo,
  getAccessToken,
  login: async (
    accessToken: string,
    refreshToken: string,
    userId: number
  ) => {},
  logout: async () => {},
  refresh: async () => {},
});

export { AuthContext, AuthState };
