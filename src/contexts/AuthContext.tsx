import { createContext } from "react";

interface AuthState {
  refreshToken: string;
  authenticated: boolean;
  userId: number;
}

async function getAuthInfo(): Promise<string> {
  return await new Promise((resolve, reject) => {
    const info = sessionStorage.getItem("authInfo") ?? "";
    resolve(info);
  });
}

async function setAuthInfo(
  accessToken: string,
  refreshToken: string,
  userId: number
): Promise<void> {
  return await new Promise((resolve, reject) => {
    sessionStorage.setItem(
      "authInfo",
      JSON.stringify({ accessToken, refreshToken, userId })
    );
    resolve();
  });
}

async function deleteAuthInfo(): Promise<void> {
  return await new Promise((resolve, reject) => {
    sessionStorage.removeItem("authInfo");
    resolve();
  });
}

async function getAccessToken(): Promise<string> {
  const jsonAuthInfo = await getAuthInfo();
  if (jsonAuthInfo !== "") {
    const authInfo = JSON.parse(jsonAuthInfo);
    return authInfo.accessToken as string;
  }
  return "";
}

let authState: AuthState = {
  refreshToken: "",
  authenticated: false,
  userId: NaN,
};

function setAuthState(state: AuthState): void {
  authState = state;
}

const AuthContext = createContext({
  authState,
  setAuthState,
  getAuthInfo,
  setAuthInfo,
  deleteAuthInfo,
  getAccessToken,
  login: async (
    accessToken: string,
    refreshToken: string,
    userId: number
  ) => {},
  logout: async () => {},
  refresh: async () => {},
});

export { AuthContext, AuthState };
