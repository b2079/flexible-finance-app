// export interface BaseObjectInfo {}

export interface BaseObjectFilters {
  id?: number[];
}

export interface BaseObject {
  id: number;
  createdAt: Date;
  updatedAt: Date;
}
