import { BaseObject } from "./BaseObject";

export enum TransactorTypeEnum {
  Account = 1,
  Entity = 2,
}

export interface TransactorType extends BaseObject {}
