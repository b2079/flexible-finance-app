import { Transactor, TransactorInfo } from "./Transactor";
import { TransactorType, TransactorTypeEnum } from "./TransactorType";
import { Field } from "./Field";
import { FieldDatum } from "./FieldDatum";
import { Group } from "./Group";
import { Tag } from "./Tag";

export class AccountInfo implements TransactorInfo {
  name = "Account";
  fieldValues: {
    [fieldId: number]: { value: string | boolean | Date; fieldDatumId: number };
  } = {};

  FieldDatumIds: number[] = [];
  FieldIds: number[] = [];
  ParentGroupId: number = NaN;
  TagIds: number[] = [];
  TemplateId: number = NaN;
  TransactorTypeId = TransactorTypeEnum.Account;
  isTemplate = false;
}

export class Account extends AccountInfo implements Transactor {
  id: number = NaN;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  FieldData?: FieldDatum[] | undefined = undefined;
  Fields?: Field[] | undefined = undefined;
  Tags?: Tag[] | undefined = undefined;
  Template?: Account | undefined = undefined;
  ParentGroup?: Group | undefined = undefined;
  TransactorType?: TransactorType = undefined;
}
