import { BaseObject, BaseObjectFilters } from "./BaseObject";
import { Field } from "./Field";
import { FieldDatum } from "./FieldDatum";
import { Tag } from "./Tag";

export interface TemplatedObjectFilters extends BaseObjectFilters {
  isTemplate?: boolean;
  FieldIds?: number[];
  TagIds?: number[];
  TemplateIds?: number[];
}

export interface TemplatedObjectInfo {
  fieldValues: {
    [fieldId: number]: { value: Date | boolean | string; fieldDatumId: number };
  };
  isTemplate: boolean;
  FieldDatumIds: number[];
  FieldIds: number[];
  TagIds: number[];
  TemplateId: number;
}

export interface TemplatedObject extends TemplatedObjectInfo, BaseObject {
  FieldData?: FieldDatum[];
  Fields?: Field[];
  Tags?: Tag[];
  Template?: TemplatedObject;
}
