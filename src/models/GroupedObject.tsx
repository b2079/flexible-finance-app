import {
  NamedObject,
  NamedObjectFilters,
  NamedObjectInfo,
} from "./NamedObject";
import { Group } from "./Group";
import { Tag } from "./Tag";

export interface GroupedObjectFilters extends NamedObjectFilters {
  ParentGroupIds?: number[];
  TagIds?: number[];
}

export interface GroupedObjectInfo extends NamedObjectInfo {
  ParentGroupId?: number;
  TagIds: number[];
}

export interface GroupedObject extends GroupedObjectInfo, NamedObject {
  ParentGroup?: Group;
  Tags?: Tag[];
}
