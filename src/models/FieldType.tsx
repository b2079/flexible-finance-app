import { NamedObject, NamedObjectInfo } from "./NamedObject";

export enum FieldTypeTypeEnum {
  Text = "Text",
  Number = "Number",
  Date = "Date",
  Boolean = "Boolean",
}

export interface FieldTypeInfo extends NamedObjectInfo {
  type?: string;
  validator?: string;
  FieldTypeComponentIds: number[];
}

export interface FieldType extends FieldTypeInfo, NamedObject {
  FieldIds: number[];
}
