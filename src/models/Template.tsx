import { NamedObject, NamedObjectInfo } from "./NamedObject";
import { Account } from "./Account";
import { Entity } from "./Entity";
import { Field } from "./Field";
import { Tag } from "./Tag";
import { Transaction } from "./Transaction";

export enum TemplateTypeEnum {
  Account = "Account",
  Entity = "Entity",
  Transaction = "Transaction",
}

export interface TemplateBaseInfo extends NamedObjectInfo {
  FieldIds: number[];
  TagIds: number[];
  type: TemplateTypeEnum;
  isTemplate: boolean;
}

export interface TemplateBase extends TemplateBaseInfo, NamedObject {
  Fields?: Field[];
  Tags?: Tag[];
}

export class AccountTemplate extends Account implements TemplateBase {
  override name = "Account Template";
  override isTemplate = true;
  override TemplateId = NaN;
  override Template = undefined;
  AccountIds = [];
  Accounts = [];
  type = TemplateTypeEnum.Account;
}

export class EntityTemplate extends Entity implements TemplateBase {
  override name = "Entity Template";
  override isTemplate = true;
  override TemplateId = NaN;
  override Template = undefined;
  EntityIds = [];
  Entities = [];
  type = TemplateTypeEnum.Entity;
}

export class TransactionTemplate extends Transaction implements TemplateBase {
  name: string = "Transaction Template";
  override isTemplate = true;
  override TemplateId = NaN;
  override Template = undefined;
  TransactionIds = [];
  Transactions = [];
  type = TemplateTypeEnum.Transaction;
}
