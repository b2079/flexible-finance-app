import {
  TemplatedObject,
  TemplatedObjectFilters,
  TemplatedObjectInfo,
} from "./TemplatedObject";
import { Field } from "./Field";
import { FieldDatum } from "./FieldDatum";
import { Tag } from "./Tag";
import { Transactor } from "./Transactor";

export interface TransactionFilters extends TemplatedObjectFilters {
  SourceTransactorIds?: number[];
  RecipientTransactorIds?: number[];
}

export class TransactionInfo implements TemplatedObjectInfo {
  fieldValues: {
    [fieldId: number]: { value: string | boolean | Date; fieldDatumId: number };
  } = {};

  FieldDatumIds: number[] = [];
  FieldIds: number[] = [];
  TagIds: number[] = [];
  TemplateId: number = NaN;
  isTemplate = false;
  SourceTransactorId: number = NaN;
  RecipientTransactorId: number = NaN;
}

export class Transaction extends TransactionInfo implements TemplatedObject {
  id: number = NaN;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  FieldData?: FieldDatum[] | undefined = undefined;
  Fields?: Field[] | undefined = undefined;
  Tags?: Tag[] | undefined = undefined;
  Template?: Transaction | undefined = undefined;
  SourceTransactor?: Transactor = undefined;
  RecipientTransactor?: Transactor = undefined;
}
