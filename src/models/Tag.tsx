import { NamedObject, NamedObjectInfo } from "./NamedObject";

export interface TagInfo extends NamedObjectInfo {}

export interface Tag extends TagInfo, NamedObject {
  AccountIds: number[];
  EntityIds: number[];
  TemplateIds: number[];
  TransactionIds: number[];
}
