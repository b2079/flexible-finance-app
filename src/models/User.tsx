import { BaseObject } from "./BaseObject";
import { Role } from "./Role";

export interface UserInfo {
  RoleIds: number[];
}

export interface User extends UserInfo, BaseObject {
  username: string;
  email: string;
  password: string;
  Roles?: Role[];
}
