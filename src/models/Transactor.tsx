import {
  GroupedObject,
  GroupedObjectFilters,
  GroupedObjectInfo,
} from "./GroupedObject";
import {
  TemplatedObject,
  TemplatedObjectFilters,
  TemplatedObjectInfo,
} from "./TemplatedObject";
import { Account } from "./Account";
import { Entity } from "./Entity";
import { TransactorType } from "./TransactorType";

export interface TransactorFilters
  extends TemplatedObjectFilters,
    GroupedObjectFilters {
  TransactorTypeIds?: number[];
}

export interface TransactorInfo extends TemplatedObjectInfo, GroupedObjectInfo {
  TransactorTypeId: number;
}

export interface Transactor
  extends TransactorInfo,
    TemplatedObject,
    GroupedObject {
  TransactorType?: TransactorType;
  Account?: Account;
  Entity?: Entity;
}
