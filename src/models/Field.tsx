import { NamedObject, NamedObjectInfo } from "./NamedObject";
import { FieldType } from "./FieldType";

export interface FieldInfo extends NamedObjectInfo {
  FieldTypeId: number;
}

export interface Field extends FieldInfo, NamedObject {
  AccountIds: number[];
  EntityIds: number[];
  TemplateIds: number[];
  TransactionIds: number[];
  FieldType?: FieldType;
}
