import { NamedObject, NamedObjectInfo } from "./NamedObject";

export interface RoleInfo extends NamedObjectInfo {
  UserIds: number[];
}

export interface Role extends RoleInfo, NamedObject {
  Users?: Role[];
}
