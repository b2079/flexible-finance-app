import { GroupedObject, GroupedObjectInfo } from "./GroupedObject";

export interface GroupInfo extends GroupedObjectInfo {}

export interface Group extends GroupInfo, GroupedObject {}
