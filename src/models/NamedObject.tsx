import { BaseObject, BaseObjectFilters } from "./BaseObject";

export interface NamedObjectFilters extends BaseObjectFilters {
  name?: string;
}

export interface NamedObjectInfo {
  name: string;
}

export interface NamedObject extends BaseObject, NamedObjectInfo {}
