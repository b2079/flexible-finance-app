import { BaseObject } from "./BaseObject";
import { Field } from "./Field";

export interface FieldDatumInfo {
  FieldId: number;
}

export interface FieldDatum extends FieldDatumInfo, BaseObject {
  booleanValue?: boolean;
  dateValue?: Date;
  numberValue?: number;
  stringValue?: string;
  Field?: Field;
}
