import { Transactor, TransactorInfo } from "./Transactor";
import { TransactorType, TransactorTypeEnum } from "./TransactorType";
import { Field } from "./Field";
import { FieldDatum } from "./FieldDatum";
import { Group } from "./Group";
import { Tag } from "./Tag";

export class EntityInfo implements TransactorInfo {
  name = "Entity";
  fieldValues: {
    [fieldId: number]: { value: string | boolean | Date; fieldDatumId: number };
  } = {};

  FieldDatumIds: number[] = [];
  FieldIds: number[] = [];
  ParentGroupId: number = NaN;
  TagIds: number[] = [];
  TemplateId: number = NaN;
  TransactorTypeId = TransactorTypeEnum.Entity;
  isTemplate = false;
}

export class Entity extends EntityInfo implements Transactor {
  id: number = NaN;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  FieldData?: FieldDatum[] | undefined = undefined;
  Fields?: Field[] | undefined = undefined;
  Tags?: Tag[] | undefined = undefined;
  Template?: Entity | undefined = undefined;
  ParentGroup?: Group | undefined = undefined;
  TransactorType?: TransactorType = undefined;
}
