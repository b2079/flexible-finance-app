import { FieldType, FieldTypeInfo } from "../models/FieldType";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface FieldTypeResponse {
  data: {
    message: string;
    fieldType: FieldType;
  };
}

interface FieldTypesResponse {
  data: {
    message: string;
    fieldTypes: FieldType[];
  };
}

export async function getFieldType(
  axiosContext: AxiosContextInterface,
  fieldTypeId: number
): Promise<FieldType> {
  const response: FieldTypeResponse = await axiosContext.privateAxios.get(
    `/fieldType/${fieldTypeId}`,
    {}
  );
  return response.data.fieldType;
}

export async function getFieldTypes(
  axiosContext: AxiosContextInterface
): Promise<FieldType[]> {
  const response: FieldTypesResponse = await axiosContext.privateAxios.get(
    `/fieldTypes`,
    {}
  );
  return response.data.fieldTypes;
}

export async function createFieldType(
  axiosContext: AxiosContextInterface,
  fieldTypeInfo: FieldTypeInfo
): Promise<FieldType> {
  const response: FieldTypeResponse = await axiosContext.privateAxios.post(
    `/fieldType`,
    fieldTypeInfo
  );
  return response.data.fieldType;
}

export async function updateFieldType(
  axiosContext: AxiosContextInterface,
  fieldTypeId: number,
  fieldTypeInfo: FieldTypeInfo
): Promise<FieldType> {
  const response: FieldTypeResponse = await axiosContext.privateAxios.put(
    `/fieldType/${fieldTypeId}`,
    fieldTypeInfo
  );
  return response.data.fieldType;
}
