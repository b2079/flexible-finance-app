import {
  TemplateBase,
  TemplateBaseInfo,
  TemplateTypeEnum,
} from "../models/Template";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface TemplateFilters {
  ids?: number[];
  name?: string;
  FieldIds?: number[];
  TagIds?: number[];
  AccountIds?: number[];
  EntityIds?: number[];
  TransactionIds?: number[];
}

interface TemplateResponse {
  data: {
    message: string;
    template: TemplateBase;
  };
}

interface TemplatesResponse {
  data: {
    message: string;
    templates: TemplateBase[];
  };
}

function getSingularEndpoint(type: TemplateTypeEnum): string {
  return "/" + type.toString().toLowerCase();
}

function getPluralEndpoint(type: TemplateTypeEnum): string {
  let endpoint = "";
  switch (type) {
    case TemplateTypeEnum.Account:
      endpoint = "/accounts";
      break;
    case TemplateTypeEnum.Entity:
      endpoint = "/entities";
      break;
    case TemplateTypeEnum.Transaction:
      endpoint = "/transactions";
      break;
    default:
      throw new Error("No such templated object.");
  }
  return endpoint;
}

export async function getTemplate(
  axiosContext: AxiosContextInterface,
  templateId: number,
  type: TemplateTypeEnum
): Promise<TemplateBase> {
  const queryEndpoint = getSingularEndpoint(type);
  const response: TemplateResponse = await axiosContext.privateAxios.get(
    `${queryEndpoint}/${templateId}`,
    {}
  );
  return response.data.template;
}

export async function getTemplates(
  axiosContext: AxiosContextInterface,
  type: TemplateTypeEnum,
  filters?: TemplateFilters
): Promise<TemplateBase[]> {
  const queryEndpoint = getPluralEndpoint(type);
  const response: TemplatesResponse = await axiosContext.privateAxios.get(
    queryEndpoint,
    {
      params: {
        isTemplate: true,
      },
    }
  );
  const templates = response.data.templates;
  return templates !== undefined
    ? templates.map((template) => {
        template.type = type;
        return template;
      })
    : [];
}

export async function createTemplate(
  axiosContext: AxiosContextInterface,
  templateInfo: TemplateBaseInfo
): Promise<TemplateBase> {
  const queryEndpoint = getSingularEndpoint(templateInfo.type);
  const response: TemplateResponse = await axiosContext.privateAxios.post(
    queryEndpoint,
    templateInfo
  );
  return response.data.template;
}

export async function updateTemplate(
  axiosContext: AxiosContextInterface,
  templateId: number,
  templateInfo: TemplateBaseInfo
): Promise<TemplateBase> {
  const queryEndpoint = getSingularEndpoint(templateInfo.type);
  const response: TemplateResponse = await axiosContext.privateAxios.put(
    `${queryEndpoint}/${templateId}`,
    templateInfo
  );
  return response.data.template;
}
