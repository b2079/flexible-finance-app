import { Entity, EntityInfo } from "../models/Entity";
import { AxiosContextInterface } from "../contexts/AxiosContext";
import { TransactorFilters } from "../models/Transactor";

interface EntityResponse {
  data: {
    message: string;
    entity: Entity;
  };
}

interface EntitysResponse {
  data: {
    message: string;
    entities: Entity[];
  };
}

export async function getEntity(
  axiosContext: AxiosContextInterface,
  entityId: number
): Promise<Entity> {
  const response: EntityResponse = await axiosContext.privateAxios.get(
    `/entity/${entityId}`,
    {}
  );
  return response.data.entity;
}

export async function getEntities(
  axiosContext: AxiosContextInterface,
  filters: TransactorFilters
): Promise<Entity[]> {
  const response: EntitysResponse = await axiosContext.privateAxios.get(
    `/entities`,
    {
      params: filters,
    }
  );
  return response.data.entities;
}

export async function createEntity(
  axiosContext: AxiosContextInterface,
  entityInfo: EntityInfo
): Promise<Entity> {
  const response: EntityResponse = await axiosContext.privateAxios.post(
    `/entity`,
    entityInfo
  );
  return response.data.entity;
}

export async function updateEntity(
  axiosContext: AxiosContextInterface,
  entityId: number,
  entityInfo: EntityInfo
): Promise<Entity> {
  const response: EntityResponse = await axiosContext.privateAxios.put(
    `/entity/${entityId}`,
    entityInfo
  );
  return response.data.entity;
}
