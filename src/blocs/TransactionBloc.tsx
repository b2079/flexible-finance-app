import {
  Transaction,
  TransactionFilters,
  TransactionInfo,
} from "../models/Transaction";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface TransactionResponse {
  data: {
    message: string;
    transaction: Transaction;
  };
}

interface TransactionsResponse {
  data: {
    message: string;
    transactions: Transaction[];
  };
}

export async function getTransaction(
  axiosContext: AxiosContextInterface,
  transactionId: number
): Promise<Transaction> {
  const response: TransactionResponse = await axiosContext.privateAxios.get(
    `/transaction/${transactionId}`,
    {}
  );
  return response.data.transaction;
}

export async function getTransactions(
  axiosContext: AxiosContextInterface,
  filters: TransactionFilters
): Promise<Transaction[]> {
  const response: TransactionsResponse = await axiosContext.privateAxios.get(
    "/transactions",
    {
      params: filters,
    }
  );
  return response.data.transactions;
}

export async function createTransaction(
  axiosContext: AxiosContextInterface,
  transactionInfo: TransactionInfo
): Promise<Transaction> {
  const response: TransactionResponse = await axiosContext.privateAxios.post(
    `/transaction`,
    transactionInfo
  );
  return response.data.transaction;
}

export async function updateTransaction(
  axiosContext: AxiosContextInterface,
  transactionId: number,
  transactionInfo: TransactionInfo
): Promise<Transaction> {
  const response: TransactionResponse = await axiosContext.privateAxios.put(
    `/transaction/${transactionId}`,
    transactionInfo
  );
  return response.data.transaction;
}
