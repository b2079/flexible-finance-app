import { Account, AccountInfo } from "../models/Account";
import { AxiosContextInterface } from "../contexts/AxiosContext";
import { TransactorFilters } from "../models/Transactor";

interface AccountResponse {
  data: {
    message: string;
    account: Account;
  };
}
interface AccountsResponse {
  data: {
    message: string;
    accounts: Account[];
  };
}

export async function getAccount(
  axiosContext: AxiosContextInterface,
  accountId: number
): Promise<Account> {
  const response: AccountResponse = await axiosContext.privateAxios.get(
    `/account/${accountId}`,
    {}
  );
  return response.data.account;
}

export async function getAccounts(
  axiosContext: AxiosContextInterface,
  filters?: TransactorFilters
): Promise<Account[]> {
  const response: AccountsResponse = await axiosContext.privateAxios.get(
    `/accounts`,
    {
      params: filters,
    }
  );
  return response.data.accounts;
}

export async function createAccount(
  axiosContext: AxiosContextInterface,
  accountInfo: AccountInfo
): Promise<Account> {
  const response: AccountResponse = await axiosContext.privateAxios.post(
    `/account`,
    accountInfo
  );
  return response.data.account;
}

export async function updateAccount(
  axiosContext: AxiosContextInterface,
  accountId: number,
  accountInfo: any
): Promise<Account> {
  const response: AccountResponse = await axiosContext.privateAxios.put(
    `/account/${accountId}`,
    accountInfo
  );
  return response.data.account;
}
