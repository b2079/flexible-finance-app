import { AxiosContextInterface } from "../contexts/AxiosContext";
import { User } from "../models/User";

interface UserResponse {
  data: {
    message: string;
    user: User;
  };
}

export async function getUser(
  axiosContext: AxiosContextInterface,
  userId: number
): Promise<User> {
  const response: UserResponse = await axiosContext.privateAxios.get(
    `/user/${userId}`,
    {}
  );
  return response.data.user;
}
