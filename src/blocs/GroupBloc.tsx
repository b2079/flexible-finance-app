import { Group, GroupInfo } from "../models/Group";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface GroupResponse {
  data: {
    message: string;
    group: Group;
  };
}

interface GroupsResponse {
  data: {
    message: string;
    groups: Group[];
  };
}

export async function getGroup(
  axiosContext: AxiosContextInterface,
  groupId: number
): Promise<Group> {
  const response: GroupResponse = await axiosContext.privateAxios.get(
    `/groups/${groupId}`,
    {}
  );
  return response.data.group;
}

export async function getGroups(
  axiosContext: AxiosContextInterface
): Promise<Group[]> {
  const response: GroupsResponse = await axiosContext.privateAxios.get(
    `/groups`,
    {}
  );
  return response.data.groups;
}

export async function createGroup(
  axiosContext: AxiosContextInterface,
  groupInfo: GroupInfo
): Promise<Group> {
  const response: GroupResponse = await axiosContext.privateAxios.post(
    `/group`,
    groupInfo
  );
  return response.data.group;
}

export async function updateGroup(
  axiosContext: AxiosContextInterface,
  groupId: number,
  groupInfo: GroupInfo
): Promise<Group> {
  const response: GroupResponse = await axiosContext.privateAxios.put(
    `/group/${groupId}`,
    groupInfo
  );
  return response.data.group;
}
