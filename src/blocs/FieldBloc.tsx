import { Field, FieldInfo } from "../models/Field";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface FieldResponse {
  data: {
    message: string;
    field: Field;
  };
}
interface FieldsResponse {
  data: {
    message: string;
    fields: Field[];
  };
}

export async function getField(
  axiosContext: AxiosContextInterface,
  fieldId: number
): Promise<Field> {
  const response: FieldResponse = await axiosContext.privateAxios.get(
    `/field/${fieldId}`,
    {}
  );
  return response.data.field;
}

export async function getFields(
  axiosContext: AxiosContextInterface
): Promise<Field[]> {
  const response: FieldsResponse = await axiosContext.privateAxios.get(
    `/fields`,
    {}
  );
  return response.data.fields;
}

export async function createField(
  axiosContext: AxiosContextInterface,
  fieldInfo: FieldInfo
): Promise<Field> {
  const response: FieldResponse = await axiosContext.privateAxios.post(
    `/field`,
    fieldInfo
  );
  return response.data.field;
}

export async function updateField(
  axiosContext: AxiosContextInterface,
  fieldId: number,
  fieldInfo: FieldInfo
): Promise<Field> {
  const response: FieldResponse = await axiosContext.privateAxios.put(
    `/field/${fieldId}`,
    fieldInfo
  );
  return response.data.field;
}
