import { Tag, TagInfo } from "../models/Tag";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface TagResponse {
  data: {
    message: string;
    tag: Tag;
  };
}
interface TagsResponse {
  data: {
    message: string;
    tags: Tag[];
  };
}

export async function getTag(
  axiosContext: AxiosContextInterface,
  tagId: number
): Promise<Tag> {
  const response: TagResponse = await axiosContext.privateAxios.get(
    `/tag/${tagId}`,
    {}
  );
  return response.data.tag;
}

export async function getTags(
  axiosContext: AxiosContextInterface
): Promise<Tag[]> {
  const response: TagsResponse = await axiosContext.privateAxios.get(
    `/tags`,
    {}
  );
  return response.data.tags;
}

export async function createTag(
  axiosContext: AxiosContextInterface,
  tagInfo: TagInfo
): Promise<Tag> {
  const response: TagResponse = await axiosContext.privateAxios.post(
    `/tag`,
    tagInfo
  );
  return response.data.tag;
}

export async function updateTag(
  axiosContext: AxiosContextInterface,
  tagId: number,
  tagInfo: TagInfo
): Promise<Tag> {
  const response: TagResponse = await axiosContext.privateAxios.put(
    `/tag/${tagId}`,
    tagInfo
  );
  return response.data.tag;
}
