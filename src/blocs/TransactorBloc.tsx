import {
  Transactor,
  TransactorFilters,
  TransactorInfo,
} from "../models/Transactor";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface TransactorResponse {
  data: {
    message: string;
    transactor: Transactor;
  };
}

interface TransactorsResponse {
  data: {
    message: string;
    transactors: Transactor[];
  };
}

export async function getTransactor(
  axiosContext: AxiosContextInterface,
  transactorId: number
): Promise<Transactor> {
  const response: TransactorResponse = await axiosContext.privateAxios.get(
    `/transactor/${transactorId}`,
    {}
  );
  return response.data.transactor;
}

export async function getTransactors(
  axiosContext: AxiosContextInterface,
  filters?: TransactorFilters
): Promise<Transactor[]> {
  const response: TransactorsResponse = await axiosContext.privateAxios.get(
    "/transactors",
    {
      params: filters,
    }
  );
  return response.data.transactors;
}

export async function createTransactor(
  axiosContext: AxiosContextInterface,
  transactorInfo: TransactorInfo
): Promise<Transactor> {
  const response: TransactorResponse = await axiosContext.privateAxios.post(
    `/transactor`,
    transactorInfo
  );
  return response.data.transactor;
}

export async function updateTransactor(
  axiosContext: AxiosContextInterface,
  transactorId: number,
  transactorInfo: TransactorInfo
): Promise<Transactor> {
  const response: TransactorResponse = await axiosContext.privateAxios.put(
    `/transactor/${transactorId}`,
    transactorInfo
  );
  return response.data.transactor;
}
