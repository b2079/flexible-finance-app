import { FieldDatum, FieldDatumInfo } from "../models/FieldDatum";
import { AxiosContextInterface } from "../contexts/AxiosContext";

interface FieldDatumResponse {
  data: {
    message: string;
    fieldDatum: FieldDatum;
  };
}

interface FieldDataResponse {
  data: {
    message: string;
    fieldData: FieldDatum[];
  };
}

export async function getFieldDatum(
  axiosContext: AxiosContextInterface,
  fieldDatumId: number
): Promise<FieldDatum> {
  const response: FieldDatumResponse = await axiosContext.privateAxios.get(
    `/fieldDatum/${fieldDatumId}`,
    {}
  );
  return response.data.fieldDatum;
}

export async function getFieldData(
  axiosContext: AxiosContextInterface
): Promise<FieldDatum[]> {
  const response: FieldDataResponse = await axiosContext.privateAxios.get(
    `/fieldData`,
    {}
  );
  return response.data.fieldData;
}

export async function createFieldDatum(
  axiosContext: AxiosContextInterface,
  fieldDatumInfo: FieldDatumInfo
): Promise<FieldDatum> {
  const response: FieldDatumResponse = await axiosContext.privateAxios.put(
    `/fieldDatum`,
    fieldDatumInfo
  );
  return response.data.fieldDatum;
}

export async function updateFieldDatum(
  axiosContext: AxiosContextInterface,
  fieldDatumId: number,
  fieldDatumInfo: FieldDatumInfo
): Promise<FieldDatum> {
  const response: FieldDatumResponse = await axiosContext.privateAxios.post(
    `/fieldDatum/${fieldDatumId}`,
    fieldDatumInfo
  );
  return response.data.fieldDatum;
}
