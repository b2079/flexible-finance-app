import * as React from "react";
import { Entity, EntityInfo } from "../../models/Entity";
import { Pressable, StyleSheet, View } from "react-native";
import {
  createEntity,
  getEntities,
  updateEntity,
} from "../../blocs/EntityBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FieldDatum } from "../../models/FieldDatum";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewEntityScreen from "./NewEntityScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";
import { getFieldDatum } from "../../blocs/FieldDatumBloc";

function EntitiesScreen({
  navigation,
}: HomeDrawerScreenProps<"Entities">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [entities, setEntities] = useState<Entity[]>();
  const [isVisibleEntityInputDialog, setIsVisibleEntityInputDialog] =
    useState(false);
  const [requestedEntity, setRequestedEntity] = useState<Entity | null>(null);
  const [requestedEntityFieldValues, setRequestedEntityFieldValues] = useState<{
    [fieldId: number]: {
      value: Date | boolean | string;
      fieldDatumId: number;
    };
  }>({});
  const [objectValues, setObjectValues] = useState<EntityInfo>({
    name: "",
    fieldValues: {},
    FieldDatumIds: [],
    FieldIds: [],
    ParentGroupId: NaN,
    TagIds: [],
    TemplateId: NaN,
    TransactorTypeId: NaN,
    isTemplate: false,
  });
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Entity");

  useEffect(() => {
    async function loadFieldValues(): Promise<void> {
      const fieldValues: {
        [fieldId: number]: {
          value: Date | boolean | string;
          fieldDatumId: number;
        };
      } = {};
      const fieldValuePromises: Array<Promise<FieldDatum>> = [];
      if (requestedEntity !== null) {
        requestedEntity.FieldDatumIds.forEach((fieldDatumId): void => {
          fieldValuePromises.push(getFieldDatum(axiosContext, fieldDatumId));
        });
        const fieldData = await Promise.all(fieldValuePromises);
        fieldData.forEach((fieldDatum) => {
          if (fieldDatum.stringValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.stringValue,
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.numberValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.numberValue.toString(),
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.booleanValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.booleanValue,
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.dateValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.dateValue,
              fieldDatumId: fieldDatum.id,
            };
          } else {
            throw Error(
              `Field Datum ${fieldDatum.id} for Field ${fieldDatum.FieldId} on Entity "${requestedEntity.name}" has no value.`
            );
          }
        });
      }
      setRequestedEntityFieldValues(fieldValues);
    }

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFieldValues();
  }, [axiosContext, requestedEntity]);

  useEffect(() => {
    const objectValues: EntityInfo = {
      name: requestedEntity?.name ?? "",
      fieldValues: requestedEntityFieldValues,
      FieldDatumIds: requestedEntity?.FieldDatumIds ?? [],
      FieldIds: requestedEntity?.FieldIds ?? [],
      ParentGroupId: requestedEntity?.ParentGroupId ?? NaN,
      TagIds: requestedEntity?.TagIds ?? [],
      TemplateId: requestedEntity?.TemplateId ?? NaN,
      TransactorTypeId: requestedEntity?.TransactorTypeId ?? NaN,
      isTemplate: requestedEntity?.isTemplate ?? false,
    };
    setObjectValues(objectValues);
  }, [requestedEntity, requestedEntityFieldValues]);

  function showEntityInputDialog(entity: Entity): void {
    setRequestedEntity(entity);
    setPrimaryButton("Update");
    setTitle(entity.name);
    setIsVisibleEntityInputDialog(true);
  }

  function showNewEntityInputDialog(): void {
    setRequestedEntity(null);
    setPrimaryButton("Create");
    setTitle("New Entity");
    setIsVisibleEntityInputDialog(true);
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewEntityInputDialog),
    });
  });

  function hideEntityInputDialog(): void {
    setIsVisibleEntityInputDialog(false);
  }

  useEffect(() => {
    async function loadEntities(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const entitiesInfo = await getEntities(axiosContext, {
          isTemplate: false,
        });
        screenStatus.current = "idle";
        setEntities(entitiesInfo);
      } catch {
        screenStatus.current = "idle";
      }
    }

    if (!isVisibleEntityInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadEntities();
    }

    return function cleanup() {
      screenStatus.current = "idle";
    };
  }, [axiosContext, isVisibleEntityInputDialog]);

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleEntityInputDialog}
        toggleVisibilty={hideEntityInputDialog}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: EntityInfo) => {
          if (primaryButton === "Create") {
            await createEntity(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateEntity(
              axiosContext,
              requestedEntity?.id ?? NaN,
              values
            );
          }
        }}
        InputScreen={NewEntityScreen}
        title={title}
        objectValues={objectValues}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={entities}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showEntityInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default EntitiesScreen;
