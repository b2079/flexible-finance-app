import * as React from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { useContext, useEffect, useRef, useState } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { AxiosContext } from "../../contexts/AxiosContext";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";
import { User } from "../../models/User";
import { getUser } from "../../blocs/UserBloc";

function ProfileScreen({
  navigation,
}: HomeDrawerScreenProps<"Profile">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const authContext = useContext(AuthContext);
  const screenStatus = useRef("idle");
  const [user, setUser] = useState<User>();

  useEffect(() => {
    async function loadUser(): Promise<void> {
      try {
        if (user === undefined || user.id === undefined) {
          const userInfo = await getUser(
            axiosContext,
            authContext.authState.userId
          );
          setUser(userInfo);
          screenStatus.current = "idle";
        }
      } catch (error: any) {
        console.log(error.message);
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadUser();

    return function cleanup() {
      screenStatus.current = "idle";
    };
  });

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View>
          <Text>ID: {user?.id}</Text>
          <Text>Email: {user?.email}</Text>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default ProfileScreen;
