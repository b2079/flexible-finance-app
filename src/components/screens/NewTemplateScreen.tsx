import * as React from "react";
import * as Yup from "yup";
import { ErrorMessage, FormikProps } from "formik";
import { IconButton, Text, TextInput } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import { TemplateBaseInfo, TemplateTypeEnum } from "../../models/Template";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { Dropdown } from "react-native-element-dropdown";
import { FlatList } from "react-native-gesture-handler";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { getFields } from "../../blocs/FieldBloc";
import { getTags } from "../../blocs/TagBloc";

interface NewTemplateProps
  extends FormikProps<TemplateBaseInfo>,
    FormikSetupProps {}

function TemplateDetailScreen(props: NewTemplateProps): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [currentSelectedField, setCurrentSelectedField] = useState<{
    label: string;
    id: number;
  } | null>(null);
  const [currentSelectedTag, setCurrentSelectedTag] = useState<{
    label: string;
    id: number;
  } | null>(null);
  const [fieldDropdownItems, setFieldDropdownItems] = useState<
    Array<{ label: string; id: number }>
  >([]);
  const [tagDropdownItems, setTagDropdownItems] = useState<
    Array<{ label: string; id: number }>
  >([]);
  const setValidationSchema = props.setValidationSchema;

  useEffect(() => {
    if (setValidationSchema !== undefined) {
      const validationSchema = Yup.object().shape({
        name: Yup.string().required("Name is required."),
      });
      setValidationSchema(validationSchema);
    }
  }, [setValidationSchema]);

  useEffect(() => {
    async function loadFields(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const fields = await getFields(axiosContext);
        screenStatus.current = "idle";
        const items = fields.map((f: any) => {
          return { label: f.name, id: f.id };
        });
        setFieldDropdownItems(items);
      } catch {
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFields();
  }, [axiosContext]);

  useEffect(() => {
    async function loadTags(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const tags = await getTags(axiosContext);
        screenStatus.current = "idle";
        const items = tags.map((t: any) => {
          return { label: t.name, id: t.id };
        });
        setTagDropdownItems(items);
      } catch {
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadTags();
  }, [axiosContext]);

  function selectField(): void {
    if (currentSelectedField !== null) {
      const fieldIds = props.values.FieldIds ?? [];
      fieldIds.push(currentSelectedField.id);
      props.values.FieldIds = fieldIds;
      setCurrentSelectedField(null);
    }
  }

  function selectTag(): void {
    if (currentSelectedTag !== null) {
      const tagIds = props.values.TagIds ?? [];
      tagIds.push(currentSelectedTag.id);
      props.values.TagIds = tagIds;
      setCurrentSelectedTag(null);
    }
  }

  return (
    <View style={styles.form}>
      <TextInput
        style={styles.input}
        placeholder="Template Name"
        autoCapitalize="words"
        onChangeText={props.handleChange("name")}
        onBlur={props.handleBlur("name")}
        value={props.values.name}
      />
      <ErrorMessage
        name="name"
        render={(msg) => <Text style={styles.input}>{msg}</Text>}
      />

      <Dropdown
        data={Object.keys(TemplateTypeEnum).map((e) => {
          return { label: e, value: e };
        })}
        labelField={"label"}
        valueField={"value"}
        value={props.values.type}
        onChange={function (item: any): void {
          props.setFieldValue("type", item.value);
        }}
        placeholder={"Pick a Type"}
      />

      <FlatList
        data={props.values.TagIds}
        renderItem={(renderItem) => (
          <View key={renderItem.item}>
            <Text>
              {
                tagDropdownItems.find(
                  (element) => element.id === renderItem.item
                )?.label
              }
            </Text>
            <IconButton
              icon={"delete"}
              onPress={(e) => {
                const updatedIds = props.values.TagIds.filter(
                  (id) => id !== renderItem.item
                );
                props.values.TagIds = updatedIds;
              }}
            ></IconButton>
          </View>
        )}
      ></FlatList>

      <Dropdown
        data={
          tagDropdownItems.filter((i) => !props.values.TagIds.includes(i.id)) ??
          []
        }
        labelField={"label"}
        valueField={"id"}
        value={currentSelectedTag?.id}
        onChange={function (item: { label: string; id: number }): void {
          setCurrentSelectedTag(item);
        }}
        placeholder={"Pick a Tag"}
      />
      <IconButton icon={"plus"} onPress={selectTag}></IconButton>

      <FlatList
        data={props.values.FieldIds}
        renderItem={(renderItem) => (
          <View key={renderItem.item}>
            <Text>
              {
                fieldDropdownItems.find(
                  (element) => element.id === renderItem.item
                )?.label
              }
            </Text>
            <IconButton
              icon={"delete"}
              onPress={(e) => {
                const updatedIds = props.values.FieldIds.filter(
                  (id) => id !== renderItem.item
                );
                props.values.FieldIds = updatedIds;
              }}
            ></IconButton>
          </View>
        )}
      ></FlatList>

      <Dropdown
        data={
          fieldDropdownItems.filter(
            (i) => !props.values.FieldIds.includes(i.id)
          ) ?? []
        }
        labelField={"label"}
        valueField={"id"}
        value={currentSelectedField?.id}
        onChange={function (item: { label: string; id: number }): void {
          setCurrentSelectedField(item);
        }}
        placeholder={"Pick a Field"}
      />
      <IconButton icon={"plus"} onPress={selectField}></IconButton>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 24,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default TemplateDetailScreen;
