import * as React from "react";
import * as Yup from "yup";
import { AccountTemplate, TemplateTypeEnum } from "../../models/Template";
import { Dropdown, MultiSelect } from "react-native-element-dropdown";
import { ErrorMessage, FormikProps } from "formik";
import FieldInput, {
  DateFieldProps,
  NumberFieldProps,
  SwitchFieldProps,
  TextFieldProps,
} from "../atoms/FieldInput";
import { Text, TextInput } from "react-native-paper";
import { useContext, useEffect, useState } from "react";
import { AccountInfo } from "../../models/Account";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FieldTypeTypeEnum } from "../../models/FieldType";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { Group } from "../../models/Group";
import { SingleChange } from "react-native-paper-dates/lib/typescript/Date/Calendar";
import { Tag } from "../../models/Tag";
import { getField } from "../../blocs/FieldBloc";
import { getFieldType } from "../../blocs/FieldTypeBloc";
import { getGroups } from "../../blocs/GroupBloc";
import { getTags } from "../../blocs/TagBloc";
import { getTemplates } from "../../blocs/TemplateBloc";

interface NewAccountProps extends FormikProps<AccountInfo>, FormikSetupProps {}

function AccountDetailScreen(props: NewAccountProps): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const [groupDropdownItems, setGroupDropdownItems] = useState<any[]>([]);
  const [fieldInputProps, setFieldInputProps] = useState<{
    [fieldId: number]:
      | DateFieldProps
      | NumberFieldProps
      | SwitchFieldProps
      | TextFieldProps;
  }>();
  const [tagDropdownItems, setTagDropdownItems] = useState<any[]>([]);
  const [templates, setTemplates] = useState<AccountTemplate[]>([]);
  const [templateDropdownItems, setTemplateDropdownItems] = useState<any[]>([]);
  const [templateFieldIds, setTemplateFieldIds] = useState<{
    [templateId: number]: number[];
  }>({});
  const setValidationSchema = props.setValidationSchema;

  useEffect(() => {
    if (setValidationSchema !== undefined) {
      const validationSchema = Yup.object().shape({
        name: Yup.string().required("Name is required."),
      });
      setValidationSchema(validationSchema);
    }
  }, [setValidationSchema]);

  useEffect(() => {
    async function loadGroups(): Promise<void> {
      try {
        axiosContext.toggleSpinner(true);
        const groups = await getGroups(axiosContext);
        const items = groups.map((g: Group) => {
          return { label: g.name, value: g.id };
        });
        axiosContext.toggleSpinner(false);
        setGroupDropdownItems(items);
      } catch {
        axiosContext.toggleSpinner(false);
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadGroups();
  }, [axiosContext]);

  useEffect(() => {
    async function loadTemplates(): Promise<void> {
      try {
        axiosContext.toggleSpinner(true);
        const retrievedTemplates = await getTemplates(
          axiosContext,
          TemplateTypeEnum.Account
        );
        axiosContext.toggleSpinner(false);
        setTemplates(retrievedTemplates as AccountTemplate[]);
      } catch {
        axiosContext.toggleSpinner(false);
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadTemplates();
  }, [axiosContext]);

  useEffect(() => {
    const items = templates.map((t: AccountTemplate) => {
      return { label: t.name, value: t.id };
    });
    setTemplateDropdownItems(items);
  }, [templates]);

  useEffect(() => {
    const fieldIds: { [templateId: number]: number[] } = {};
    templates.forEach((t: AccountTemplate) => {
      fieldIds[t.id] = t.FieldIds;
    });
    setTemplateFieldIds(fieldIds);
  }, [templates]);

  useEffect(() => {
    async function loadTags(): Promise<void> {
      try {
        axiosContext.toggleSpinner(true);
        const tags = await getTags(axiosContext);
        const items = tags.map((t: Tag) => {
          return { label: t.name, value: t.id };
        });
        axiosContext.toggleSpinner(false);
        setTagDropdownItems(items);
      } catch {
        axiosContext.toggleSpinner(false);
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadTags();
  }, [axiosContext]);

  useEffect(() => {
    async function loadFieldInputProps(): Promise<void> {
      if (
        isNaN(props.values.TemplateId) ||
        templateFieldIds[props.values.TemplateId] === undefined ||
        templateFieldIds[props.values.TemplateId].length === 0
      ) {
        return;
      }
      const fieldIds = templateFieldIds[props.values.TemplateId];
      const propsMap: {
        [fieldId: number]:
          | DateFieldProps
          | NumberFieldProps
          | SwitchFieldProps
          | TextFieldProps;
      } = {};
      for (const fieldId of fieldIds) {
        const field = await getField(axiosContext, fieldId);
        const fieldType = await getFieldType(axiosContext, field.FieldTypeId);
        const typeEnum = fieldType.type as FieldTypeTypeEnum;
        switch (typeEnum) {
          case FieldTypeTypeEnum.Boolean: {
            const props: SwitchFieldProps = {
              valueType: typeEnum,
            };
            propsMap[fieldId] = props;
            break;
          }
          case FieldTypeTypeEnum.Date: {
            const props: DateFieldProps = {
              locale: "en",
              mode: "single",
              valueType: typeEnum,
              onConfirm: function (params: { date: Date }): void {
                throw new Error("Function not implemented.");
              } as SingleChange,
              onDismiss: function (): void {
                throw new Error("Function not implemented.");
              },
            };
            propsMap[fieldId] = props;
            break;
          }
          case FieldTypeTypeEnum.Number: {
            const props: NumberFieldProps = {
              valueType: typeEnum,
            };
            propsMap[fieldId] = props;
            break;
          }
          case FieldTypeTypeEnum.Text: {
            const props: TextFieldProps = {
              valueType: typeEnum,
            };
            propsMap[fieldId] = props;
            break;
          }
        }
      }
      setFieldInputProps(propsMap);
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFieldInputProps();
  }, [axiosContext, props.values.TemplateId, templateFieldIds]);

  return (
    <>
      <TextInput
        placeholder="Account Name"
        autoCapitalize="words"
        onChangeText={props.handleChange("name")}
        onBlur={props.handleBlur("name")}
        value={props.values.name}
      />
      <ErrorMessage name="name" render={(msg) => <Text>{msg}</Text>} />

      <Dropdown
        data={groupDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.ParentGroupId}
        onChange={function (item: any): void {
          props.setFieldValue("GroupId", +item.value);
        }}
        placeholder={"Pick a Group"}
      />
      <Dropdown
        data={templateDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.TemplateId}
        onChange={function (item: any): void {
          props.setFieldValue("TemplateId", +item.value);
        }}
        placeholder={"Pick a Template"}
      />
      <MultiSelect
        data={tagDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.TagIds}
        onChange={function (items: number[]): void {
          props.setFieldValue("TagIds", items);
        }}
        placeholder={"Pick Tags"}
      />

      {fieldInputProps !== undefined ? (
        templateFieldIds[props.values.TemplateId].map((fieldId) => (
          <FieldInput
            key={fieldId}
            valueType={fieldInputProps[fieldId].valueType}
            onChangeValue={function (
              enteredValue: string | boolean | Date
            ): void {
              const newFieldValues = props.values.fieldValues;
              newFieldValues[fieldId] = {
                value: enteredValue,
                fieldDatumId:
                  props.values.fieldValues[fieldId]?.fieldDatumId ?? NaN,
              };
              props.setFieldValue("fieldValues", newFieldValues);
            }}
            value={props.values.fieldValues[fieldId]?.value ?? ""}
          />
        ))
      ) : (
        <></>
      )}
    </>
  );
}

export default AccountDetailScreen;
