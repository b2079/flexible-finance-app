import * as React from "react";
import { Group, GroupInfo } from "../../models/Group";
import { Pressable, StyleSheet, View } from "react-native";
import { createGroup, getGroups, updateGroup } from "../../blocs/GroupBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewGroupScreen from "./NewGroupScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";

function GroupsScreen({
  navigation,
}: HomeDrawerScreenProps<"Groups">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [groups, setGroups] = useState<Group[]>();
  const [isVisibleGroupInputDialog, setIsVisibleGroupInputDialog] =
    useState(false);
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Group");
  const [requestedGroup, setRequestedGroup] = useState<Group | null>(null);
  const [objectValues, setObjectValues] = useState<GroupInfo>({
    name: "",
    ParentGroupId: NaN,
    TagIds: [],
  });

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewGroupInputDialog),
    });
  });

  useEffect(() => {
    async function loadGroups(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const groupsInfo = await getGroups(axiosContext);
        screenStatus.current = "idle";
        setGroups(groupsInfo);
      } catch {
        screenStatus.current = "idle";
      }
    }
    if (!isVisibleGroupInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadGroups();
    }

    return function cleanup() {
      screenStatus.current = "idle";
    };
  }, [axiosContext, isVisibleGroupInputDialog]);

  useEffect(() => {
    const objectValues: GroupInfo = {
      name: requestedGroup?.name ?? "",
      ParentGroupId: requestedGroup?.ParentGroupId ?? NaN,
      TagIds: requestedGroup?.TagIds ?? [],
    };
    setObjectValues(objectValues);
  }, [requestedGroup]);

  function showGroupInputDialog(group: Group): void {
    setRequestedGroup(group);
    setPrimaryButton("Update");
    setTitle(group.name);
    setIsVisibleGroupInputDialog(true);
  }

  function showNewGroupInputDialog(): void {
    setRequestedGroup(null);
    setPrimaryButton("Create");
    setTitle("New Group");
    setIsVisibleGroupInputDialog(true);
  }

  function hideGroupInputDialog(): void {
    setIsVisibleGroupInputDialog(false);
  }

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleGroupInputDialog}
        toggleVisibilty={hideGroupInputDialog}
        title={title}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: GroupInfo) => {
          if (primaryButton === "Create") {
            await createGroup(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateGroup(axiosContext, requestedGroup?.id ?? NaN, values);
          }
        }}
        InputScreen={NewGroupScreen}
        objectValues={{ ...objectValues }}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={groups}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showGroupInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default GroupsScreen;
