import * as React from "react";
import { Account, AccountInfo } from "../../models/Account";
import { Pressable, StyleSheet, View } from "react-native";
import {
  createAccount,
  getAccounts,
  updateAccount,
} from "../../blocs/AccountBloc";
import { useContext, useEffect, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FieldDatum } from "../../models/FieldDatum";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewAccountScreen from "./NewAccountScreen";
import PlusButton from "../buttons/PlusButton";
import { Text } from "react-native-paper";
import { getFieldDatum } from "../../blocs/FieldDatumBloc";

function AccountsScreen({
  navigation,
}: HomeDrawerScreenProps<"Accounts">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const [accounts, setAccounts] = useState<Account[]>();
  const [objectValues, setObjectValues] = useState<AccountInfo>({
    name: "",
    fieldValues: {},
    FieldDatumIds: [],
    FieldIds: [],
    ParentGroupId: NaN,
    TagIds: [],
    TemplateId: NaN,
    TransactorTypeId: NaN,
    isTemplate: false,
  });
  const [isVisibleAccountInputDialog, setIsVisibleAccountInputDialog] =
    useState(false);
  const [requestedAccount, setRequestedAccount] = useState<Account | null>(
    null
  );
  const [requestedAccountFieldValues, setRequestedAccountFieldValues] =
    useState<{
      [fieldId: number]: {
        value: Date | boolean | string;
        fieldDatumId: number;
      };
    }>({});
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Account");

  useEffect(() => {
    async function loadFieldValues(): Promise<void> {
      const fieldValues: {
        [fieldId: number]: {
          value: Date | boolean | string;
          fieldDatumId: number;
        };
      } = {};
      const fieldValuePromises: Array<Promise<FieldDatum>> = [];
      if (requestedAccount !== null) {
        requestedAccount.FieldDatumIds.forEach((fieldDatumId): void => {
          fieldValuePromises.push(getFieldDatum(axiosContext, fieldDatumId));
        });
        const fieldData = await Promise.all(fieldValuePromises);
        fieldData.forEach((fieldDatum) => {
          if (fieldDatum.stringValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.stringValue,
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.numberValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.numberValue.toString(),
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.booleanValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.booleanValue,
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.dateValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.dateValue,
              fieldDatumId: fieldDatum.id,
            };
          } else {
            throw Error(
              `Field Datum ${fieldDatum.id} for Field ${fieldDatum.FieldId} on Account "${requestedAccount.name}" has no value.`
            );
          }
        });
      }
      setRequestedAccountFieldValues(fieldValues);
    }

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFieldValues();
  }, [axiosContext, requestedAccount]);

  useEffect(() => {
    const objectValues: AccountInfo = {
      name: requestedAccount?.name ?? "",
      fieldValues: requestedAccountFieldValues,
      FieldDatumIds: requestedAccount?.FieldDatumIds ?? [],
      FieldIds: requestedAccount?.FieldIds ?? [],
      ParentGroupId: requestedAccount?.ParentGroupId ?? NaN,
      TagIds: requestedAccount?.TagIds ?? [],
      TemplateId: requestedAccount?.TemplateId ?? NaN,
      TransactorTypeId: requestedAccount?.TransactorTypeId ?? NaN,
      isTemplate: requestedAccount?.isTemplate ?? false,
    };
    setObjectValues(objectValues);
  }, [requestedAccount, requestedAccountFieldValues]);

  function showAccountInputDialog(account: Account): void {
    setRequestedAccount(account);
    setPrimaryButton("Update");
    setTitle(account.name);
    setIsVisibleAccountInputDialog(true);
  }

  function showNewAccountInputDialog(): void {
    setRequestedAccount(null);
    setPrimaryButton("Create");
    setTitle("New Account");
    setIsVisibleAccountInputDialog(true);
  }

  function hideAccountInputDialog(): void {
    setIsVisibleAccountInputDialog(false);
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewAccountInputDialog),
    });
  });

  useEffect(() => {
    async function loadAccounts(): Promise<void> {
      try {
        axiosContext.toggleSpinner(true);
        const accountsInfo = await getAccounts(axiosContext, {
          isTemplate: false,
        });
        axiosContext.toggleSpinner(false);
        setAccounts(accountsInfo);
      } catch {
        axiosContext.toggleSpinner(false);
      }
    }

    if (!isVisibleAccountInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadAccounts();
    }

    return function cleanup() {
      axiosContext.toggleSpinner(false);
    };
  }, [axiosContext, isVisibleAccountInputDialog]);

  return (
    <>
      <InputDialog
        dismissable={true}
        visible={isVisibleAccountInputDialog}
        toggleVisibilty={hideAccountInputDialog}
        title={title}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: AccountInfo) => {
          if (primaryButton === "Create") {
            await createAccount(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateAccount(
              axiosContext,
              requestedAccount?.id ?? NaN,
              values
            );
          }
        }}
        InputScreen={NewAccountScreen}
        objectValues={{ ...objectValues }}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={accounts}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showAccountInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default AccountsScreen;
