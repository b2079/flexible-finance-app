import * as React from "react";
import {
  DrawerContentComponentProps,
  DrawerContentScrollView,
  createDrawerNavigator,
} from "@react-navigation/drawer";
import {
  HomeDrawerParamList,
  RootStackScreenProps,
} from "../../types/NavigationTypes";
import AccountsScreen from "./AccountsScreen";
import DashboardScreen from "./DashboardScreen";
import { Drawer } from "react-native-paper";
import EntitiesScreen from "./EntitiesScreen";
import FieldTypesScreen from "./FieldTypesScreen";
import FieldsScreen from "./FieldsScreen";
import GroupsScreen from "./GroupsScreen";
import MenuChevron from "../buttons/MenuChevronButton";
import ProfileScreen from "./ProfileScreen";
import TagsScreen from "./TagsScreen";
import TemplatesScreen from "./TemplatesScreen";
import TransactionsScreen from "./TransactionsScreen";
import { useState } from "react";

function HomeScreen({ navigation }: RootStackScreenProps<"Home">): JSX.Element {
  const DrawerNav = createDrawerNavigator<HomeDrawerParamList>();
  const [isAccountsMenuVisible, setAccountsMenuVisible] = useState(false);
  function toggleAccountsMenuVisible(): void {
    setAccountsMenuVisible(!isAccountsMenuVisible);
  }
  const [isSettingsMenuVisible, setSettingsMenuVisible] = useState(false);
  function toggleSettingsMenuVisible(): void {
    setSettingsMenuVisible(!isSettingsMenuVisible);
  }

  return (
    <DrawerNav.Navigator
      initialRouteName="Profile"
      drawerContent={DrawerContent}
    >
      <DrawerNav.Screen name="Dashboard" component={DashboardScreen} />
      <DrawerNav.Screen name="Accounts" component={AccountsScreen} />
      <DrawerNav.Screen name="Entities" component={EntitiesScreen} />
      <DrawerNav.Screen name="Fields" component={FieldsScreen} />
      <DrawerNav.Screen name="FieldTypes" component={FieldTypesScreen} />
      <DrawerNav.Screen name="Groups" component={GroupsScreen} />
      <DrawerNav.Screen name="Profile" component={ProfileScreen} />
      <DrawerNav.Screen name="Tags" component={TagsScreen} />
      <DrawerNav.Screen name="Templates" component={TemplatesScreen} />
      <DrawerNav.Screen name="Transactions" component={TransactionsScreen} />
    </DrawerNav.Navigator>
  );

  function DrawerContent(props: DrawerContentComponentProps): JSX.Element {
    return (
      <DrawerContentScrollView {...props}>
        <Drawer.Item
          label="Dashboard"
          onPress={() => props.navigation.navigate("Dashboard")}
        />
        <Drawer.Item
          label="Accounts"
          onPress={() => props.navigation.navigate("Accounts")}
          right={() =>
            MenuChevron(isAccountsMenuVisible, toggleAccountsMenuVisible)
          }
        />
        <Drawer.Item
          label="Entities"
          onPress={() => props.navigation.navigate("Entities")}
        />
        <Drawer.Item
          label="Groups"
          onPress={() => props.navigation.navigate("Groups")}
        />
        <Drawer.Item
          label="Transactions"
          onPress={() => props.navigation.navigate("Transactions")}
        />
        <Drawer.Item
          label="Profile"
          onPress={() => props.navigation.navigate("Profile")}
        />
        <Drawer.Item
          label="Settings"
          onPress={() => toggleSettingsMenuVisible()}
          right={() =>
            MenuChevron(isSettingsMenuVisible, toggleSettingsMenuVisible)
          }
        />
        {isSettingsMenuVisible && (
          <ul>
            <li>
              <Drawer.Item
                label="Fields"
                onPress={() => props.navigation.navigate("Fields")}
              />
            </li>
            <li>
              <Drawer.Item
                label="Field Types"
                onPress={() => props.navigation.navigate("FieldTypes")}
              />
            </li>
            <li>
              <Drawer.Item
                label="Tags"
                onPress={() => props.navigation.navigate("Tags")}
              />
            </li>
            <li>
              <Drawer.Item
                label="Templates"
                onPress={() => props.navigation.navigate("Templates")}
              />
            </li>
          </ul>
        )}
      </DrawerContentScrollView>
    );
  }
}

export default HomeScreen;
