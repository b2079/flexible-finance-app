import * as React from "react";
import * as Yup from "yup";
import { ErrorMessage, FormikProps } from "formik";
import { StyleSheet, View } from "react-native";
import { Text, TextInput } from "react-native-paper";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { Dropdown } from "react-native-element-dropdown";
import { FieldInfo } from "../../models/Field";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { getFieldTypes } from "../../blocs/FieldTypeBloc";

interface NewFieldProps extends FormikProps<FieldInfo>, FormikSetupProps {}

function FieldDetailScreen(props: NewFieldProps): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [fieldTypeDropdownItems, setFieldTypeDropdownItems] = useState<any[]>();
  const setValidationSchema = props.setValidationSchema;

  useEffect(() => {
    if (setValidationSchema !== undefined) {
      const validationSchema = Yup.object().shape({
        name: Yup.string().required("Name is required."),
      });
      setValidationSchema(validationSchema);
    }
  }, [setValidationSchema]);

  useEffect(() => {
    async function loadFieldTypes(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const fieldTypes = await getFieldTypes(axiosContext);
        screenStatus.current = "idle";
        const items = fieldTypes.map((t: any) => {
          return { label: t.name, value: t.id };
        });
        setFieldTypeDropdownItems(items);
      } catch {
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFieldTypes();
  }, [axiosContext]);

  return (
    <View style={styles.form}>
      <TextInput
        style={styles.input}
        placeholder="Field Name"
        autoCapitalize="words"
        onChangeText={props.handleChange("name")}
        onBlur={props.handleBlur("name")}
        value={props.values.name}
      />
      <ErrorMessage
        name="name"
        render={(msg) => <Text style={styles.input}>{msg}</Text>}
      />

      <Dropdown
        data={fieldTypeDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.FieldTypeId}
        onChange={function (item: any): void {
          props.setFieldValue("FieldTypeId", +item.value);
        }}
        placeholder={"Pick a Field Type"}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 24,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default FieldDetailScreen;
