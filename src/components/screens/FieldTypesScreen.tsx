import * as React from "react";
import { FieldType, FieldTypeInfo } from "../../models/FieldType";
import { Pressable, StyleSheet, View } from "react-native";
import {
  createFieldType,
  getFieldTypes,
  updateFieldType,
} from "../../blocs/FieldTypeBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewFieldTypeScreen from "./NewFieldTypeScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";

function FieldTypesScreen({
  navigation,
}: HomeDrawerScreenProps<"FieldTypes">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [fieldTypeTypes, setFieldTypes] = useState<FieldType[]>();
  const [isVisibleFieldTypeInputDialog, setIsVisibleFieldTypeInputDialog] =
    useState(false);
  const [objectValues, setObjectValues] = useState<FieldTypeInfo>({
    name: "",
    type: "",
    validator: "",
    FieldTypeComponentIds: [],
  });
  const [requestedFieldType, setRequestedFieldType] =
    useState<FieldType | null>(null);
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Field Type");

  useEffect(() => {
    const objectValues: FieldTypeInfo = {
      name: requestedFieldType?.name ?? "",
      type: requestedFieldType?.type ?? "",
      validator: requestedFieldType?.validator ?? "",
      FieldTypeComponentIds: requestedFieldType?.FieldTypeComponentIds ?? [],
    };
    setObjectValues(objectValues);
  }, [requestedFieldType]);

  function showFieldTypeInputDialog(fieldType: FieldType): void {
    setRequestedFieldType(fieldType);
    setPrimaryButton("Update");
    setTitle(fieldType.name);
    setIsVisibleFieldTypeInputDialog(true);
  }

  function showNewFieldTypeInputDialog(): void {
    setRequestedFieldType(null);
    setPrimaryButton("Create");
    setTitle("New Field Type");
    setIsVisibleFieldTypeInputDialog(true);
  }

  function hideFieldTypeInputDialog(): void {
    setIsVisibleFieldTypeInputDialog(false);
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewFieldTypeInputDialog),
    });
  });

  useEffect(() => {
    async function loadFieldTypes(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const fieldTypeTypesInfo = await getFieldTypes(axiosContext);
        screenStatus.current = "idle";
        setFieldTypes(fieldTypeTypesInfo);
      } catch {
        screenStatus.current = "idle";
      }
    }

    if (!isVisibleFieldTypeInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadFieldTypes();
    }

    return function cleanup() {
      screenStatus.current = "idle";
    };
  }, [axiosContext, isVisibleFieldTypeInputDialog]);

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleFieldTypeInputDialog}
        toggleVisibilty={hideFieldTypeInputDialog}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: FieldTypeInfo) => {
          if (primaryButton === "Create") {
            await createFieldType(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateFieldType(
              axiosContext,
              requestedFieldType?.id ?? NaN,
              values
            );
          }
        }}
        InputScreen={NewFieldTypeScreen}
        title={title}
        objectValues={objectValues}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={fieldTypeTypes}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showFieldTypeInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default FieldTypesScreen;
