import * as React from "react";
import { StyleSheet, View } from "react-native";
import { Button } from "react-native-paper";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";

function DashboardScreen({
  navigation,
}: HomeDrawerScreenProps<"Dashboard">): JSX.Element {
  return (
    <View style={styles.container}>
      <View style={styles.buttonGroup}>
        <Button>Get Image</Button>
        <Button>Logout</Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  image: {
    width: "90%",
    height: "50%",
    resizeMode: "contain",
  },
  buttonGroup: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "90%",
  },
});
export default DashboardScreen;
