import * as React from "react";
import { Field, FieldInfo } from "../../models/Field";
import { Pressable, StyleSheet, View } from "react-native";
import { createField, getFields, updateField } from "../../blocs/FieldBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewFieldScreen from "./NewFieldScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";

function FieldsScreen({
  navigation,
}: HomeDrawerScreenProps<"Fields">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [fields, setFields] = useState<Field[]>();
  const [isVisibleFieldInputDialog, setIsVisibleFieldInputDialog] =
    useState(false);
  const [requestedField, setRequestedField] = useState<Field | null>(null);
  const [objectValues, setObjectValues] = useState<FieldInfo>({
    name: "",
    FieldTypeId: NaN,
  });
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Field");

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewFieldInputDialog),
    });
  });

  useEffect(() => {
    const objectValues: FieldInfo = {
      name: requestedField?.name ?? "",
      FieldTypeId: requestedField?.FieldTypeId ?? NaN,
    };
    setObjectValues(objectValues);
  }, [requestedField]);

  function showFieldInputDialog(field: Field): void {
    setRequestedField(field);
    setPrimaryButton("Update");
    setTitle(field.name);
    setIsVisibleFieldInputDialog(true);
  }

  function showNewFieldInputDialog(): void {
    setRequestedField(null);
    setPrimaryButton("Create");
    setTitle("New Field");
    setIsVisibleFieldInputDialog(true);
  }

  function hideFieldInputDialog(): void {
    setIsVisibleFieldInputDialog(false);
  }

  useEffect(() => {
    async function loadFields(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const fieldsInfo = await getFields(axiosContext);
        screenStatus.current = "idle";
        setFields(fieldsInfo);
      } catch {
        screenStatus.current = "idle";
      }
    }

    if (!isVisibleFieldInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadFields();
    }

    return function cleanup() {
      screenStatus.current = "idle";
    };
  }, [axiosContext, isVisibleFieldInputDialog]);

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleFieldInputDialog}
        toggleVisibilty={hideFieldInputDialog}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: FieldInfo) => {
          if (primaryButton === "Create") {
            await createField(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateField(axiosContext, requestedField?.id ?? NaN, values);
          }
        }}
        InputScreen={NewFieldScreen}
        title={title}
        objectValues={objectValues}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={fields}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showFieldInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default FieldsScreen;
