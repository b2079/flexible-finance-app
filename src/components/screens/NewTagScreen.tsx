import * as React from "react";
import * as Yup from "yup";
import { ErrorMessage, FormikProps } from "formik";
import { StyleSheet, View } from "react-native";
import { Text, TextInput } from "react-native-paper";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { TagInfo } from "../../models/Tag";
import { useEffect } from "react";

interface NewTagProps extends FormikProps<TagInfo>, FormikSetupProps {}

function TagDetailScreen(props: NewTagProps): JSX.Element {
  const setValidationSchema = props.setValidationSchema;

  useEffect(() => {
    if (setValidationSchema !== undefined) {
      const validationSchema = Yup.object().shape({
        name: Yup.string().required("Name is required."),
      });
      setValidationSchema(validationSchema);
    }
  }, [setValidationSchema]);

  return (
    <View style={styles.form}>
      <TextInput
        style={styles.input}
        placeholder="Tag Name"
        autoCapitalize="words"
        onChangeText={props.handleChange("name")}
        onBlur={props.handleBlur("name")}
        value={props.values.name}
      />
      <ErrorMessage
        name="name"
        render={(msg) => <Text style={styles.input}>{msg}</Text>}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 24,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default TagDetailScreen;
