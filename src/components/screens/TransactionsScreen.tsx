import * as React from "react";
import { Pressable, StyleSheet, View } from "react-native";
import { Transaction, TransactionInfo } from "../../models/Transaction";
import {
  createTransaction,
  getTransactions,
  updateTransaction,
} from "../../blocs/TransactionBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FieldDatum } from "../../models/FieldDatum";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewTransactionScreen from "./NewTransactionScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";
import { getFieldDatum } from "../../blocs/FieldDatumBloc";

function TransactionsScreen({
  navigation,
}: HomeDrawerScreenProps<"Transactions">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [transactions, setTransactions] = useState<Transaction[]>();
  const [isVisibleTransactionInputDialog, setIsVisibleTransactionInputDialog] =
    useState(false);
  const [requestedTransaction, setRequestedTransaction] =
    useState<Transaction | null>(null);
  const [requestedTransactionFieldValues, setRequestedTransactionFieldValues] =
    useState<{
      [fieldId: number]: {
        value: Date | boolean | string;
        fieldDatumId: number;
      };
    }>({});
  const [objectValues, setObjectValues] = useState<TransactionInfo>({
    fieldValues: {},
    FieldDatumIds: [],
    FieldIds: [],
    RecipientTransactorId: NaN,
    SourceTransactorId: NaN,
    TagIds: [],
    TemplateId: NaN,
    isTemplate: false,
  });
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Transaction");

  useEffect(() => {
    async function loadFieldValues(): Promise<void> {
      const fieldValues: {
        [fieldId: number]: {
          value: Date | boolean | string;
          fieldDatumId: number;
        };
      } = {};
      const fieldValuePromises: Array<Promise<FieldDatum>> = [];
      if (requestedTransaction !== null) {
        requestedTransaction.FieldDatumIds.forEach((fieldDatumId): void => {
          fieldValuePromises.push(getFieldDatum(axiosContext, fieldDatumId));
        });
        const fieldData = await Promise.all(fieldValuePromises);
        fieldData.forEach((fieldDatum) => {
          if (fieldDatum.stringValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.stringValue,
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.numberValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.numberValue.toString(),
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.booleanValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.booleanValue,
              fieldDatumId: fieldDatum.id,
            };
          } else if (fieldDatum.dateValue !== undefined) {
            fieldValues[fieldDatum.FieldId] = {
              value: fieldDatum.dateValue,
              fieldDatumId: fieldDatum.id,
            };
          } else {
            throw Error(
              `Field Datum ${fieldDatum.id} for Field ${fieldDatum.FieldId} on Transaction "${requestedTransaction.id}" has no value.`
            );
          }
        });
      }
      setRequestedTransactionFieldValues(fieldValues);
    }

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFieldValues();
  }, [axiosContext, requestedTransaction]);

  useEffect(() => {
    const objectValues: TransactionInfo = {
      FieldDatumIds: requestedTransaction?.FieldDatumIds ?? [],
      FieldIds: requestedTransaction?.FieldIds ?? [],
      RecipientTransactorId: requestedTransaction?.RecipientTransactorId ?? NaN,
      SourceTransactorId: requestedTransaction?.RecipientTransactorId ?? NaN,
      TagIds: requestedTransaction?.TagIds ?? [],
      TemplateId: requestedTransaction?.TemplateId ?? NaN,
      fieldValues: requestedTransactionFieldValues,
      isTemplate: requestedTransaction?.isTemplate ?? false,
    };
    setObjectValues(objectValues);
  }, [requestedTransaction, requestedTransactionFieldValues]);

  function showTransactionInputDialog(transaction: Transaction): void {
    setRequestedTransaction(transaction);
    setPrimaryButton("Update");
    setTitle(transaction.id.toString());
    setIsVisibleTransactionInputDialog(true);
  }

  function showNewTransactionInputDialog(): void {
    setRequestedTransaction(null);
    setPrimaryButton("Create");
    setTitle("New Transaction");
    setIsVisibleTransactionInputDialog(true);
  }

  function hideTransactionInputDialog(): void {
    setIsVisibleTransactionInputDialog(false);
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewTransactionInputDialog),
    });
  });

  useEffect(() => {
    async function loadTransactions(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const transactionsInfo = await getTransactions(axiosContext, {
          isTemplate: false,
        });
        screenStatus.current = "idle";
        setTransactions(transactionsInfo);
      } catch {
        screenStatus.current = "idle";
      }
    }

    if (!isVisibleTransactionInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadTransactions();
    }

    return function cleanup() {
      screenStatus.current = "idle";
    };
  }, [axiosContext, isVisibleTransactionInputDialog]);

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleTransactionInputDialog}
        toggleVisibilty={hideTransactionInputDialog}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: TransactionInfo) => {
          if (primaryButton === "Create") {
            await createTransaction(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateTransaction(
              axiosContext,
              requestedTransaction?.id ?? NaN,
              values
            );
          }
        }}
        InputScreen={NewTransactionScreen}
        title={title}
        objectValues={objectValues}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={transactions}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showTransactionInputDialog(item)}>
              <Text>{item.id.toString()}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default TransactionsScreen;
