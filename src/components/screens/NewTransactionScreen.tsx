import * as React from "react";
import { Dropdown, MultiSelect } from "react-native-element-dropdown";
import FieldInput, {
  DateFieldProps,
  NumberFieldProps,
  SwitchFieldProps,
  TextFieldProps,
} from "../atoms/FieldInput";
import { StyleSheet, View } from "react-native";
import { TemplateTypeEnum, TransactionTemplate } from "../../models/Template";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FieldTypeTypeEnum } from "../../models/FieldType";
import { FormikProps } from "formik";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { SingleChange } from "react-native-paper-dates/lib/typescript/Date/Calendar";
import { TransactionInfo } from "../../models/Transaction";
import { Transactor } from "../../models/Transactor";
import { getField } from "../../blocs/FieldBloc";
import { getFieldType } from "../../blocs/FieldTypeBloc";
import { getTags } from "../../blocs/TagBloc";
import { getTemplates } from "../../blocs/TemplateBloc";
import { getTransactors } from "../../blocs/TransactorBloc";

interface NewTransactionProps
  extends FormikProps<TransactionInfo>,
    FormikSetupProps {}

function TransactionDetailScreen(props: NewTransactionProps): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [fieldInputProps, setFieldInputProps] = useState<{
    [fieldId: number]:
      | DateFieldProps
      | NumberFieldProps
      | SwitchFieldProps
      | TextFieldProps;
  }>();
  const [tagDropdownItems, setTagDropdownItems] = useState<any[]>([]);
  const [templates, setTemplates] = useState<TransactionTemplate[]>([]);
  const [templateDropdownItems, setTemplateDropdownItems] = useState<any[]>([]);
  const [transactors, setTransactors] = useState<Transactor[]>([]);
  const [transactorDropdownItems, setTransactorDropdownItems] = useState<any[]>(
    []
  );
  const [templateFieldIds, setTemplateFieldIds] = useState<{
    [templateId: number]: number[];
  }>({});

  useEffect(() => {
    async function loadTags(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const tags = await getTags(axiosContext);
        screenStatus.current = "idle";
        const items = tags.map((t: any) => {
          return { label: t.name, value: t.id };
        });
        setTagDropdownItems(items);
      } catch {
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadTags();
  }, [axiosContext]);

  useEffect(() => {
    async function loadTemplates(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const retrievedTemplates = await getTemplates(
          axiosContext,
          TemplateTypeEnum.Transaction
        );
        setTemplates(retrievedTemplates as TransactionTemplate[]);
      } catch {
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadTemplates();
  }, [axiosContext]);

  useEffect(() => {
    const items = templates.map((t: TransactionTemplate) => {
      return { label: t.name, value: t.id };
    });
    setTemplateDropdownItems(items);
  }, [templates]);

  useEffect(() => {
    async function loadTransactors(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const retrievedTransactors = await getTransactors(axiosContext, {
          isTemplate: false,
        });
        setTransactors(retrievedTransactors);
      } catch {
        screenStatus.current = "idle";
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadTransactors();
  }, [axiosContext]);

  useEffect(() => {
    const items = transactors.map((t: Transactor) => {
      return { label: t.name, value: t.id };
    });
    setTransactorDropdownItems(items);
  }, [transactors]);

  useEffect(() => {
    const fieldIds: { [templateId: number]: number[] } = {};
    templates.forEach((t: TransactionTemplate) => {
      fieldIds[t.id] = t.FieldIds;
    });
    setTemplateFieldIds(fieldIds);
  }, [templates]);

  useEffect(() => {
    async function loadFieldInputProps(): Promise<void> {
      if (
        isNaN(props.values.TemplateId) ||
        templateFieldIds[props.values.TemplateId] === undefined ||
        templateFieldIds[props.values.TemplateId].length === 0
      ) {
        return;
      }
      const fieldIds = templateFieldIds[props.values.TemplateId];
      const propsMap: {
        [fieldId: number]:
          | DateFieldProps
          | NumberFieldProps
          | SwitchFieldProps
          | TextFieldProps;
      } = {};
      for (const fieldId of fieldIds) {
        const field = await getField(axiosContext, fieldId);
        const fieldType = await getFieldType(axiosContext, field.FieldTypeId);
        const typeEnum = fieldType.type as FieldTypeTypeEnum;
        switch (typeEnum) {
          case FieldTypeTypeEnum.Boolean: {
            const props: SwitchFieldProps = {
              valueType: typeEnum,
            };
            propsMap[fieldId] = props;
            break;
          }
          case FieldTypeTypeEnum.Date: {
            const props: DateFieldProps = {
              locale: "en",
              mode: "single",
              valueType: typeEnum,
              onConfirm: function (params: { date: Date }): void {
                throw new Error("Function not implemented.");
              } as SingleChange,
              onDismiss: function (): void {
                throw new Error("Function not implemented.");
              },
            };
            propsMap[fieldId] = props;
            break;
          }
          case FieldTypeTypeEnum.Number: {
            const props: NumberFieldProps = {
              valueType: typeEnum,
            };
            propsMap[fieldId] = props;
            break;
          }
          case FieldTypeTypeEnum.Text: {
            const props: TextFieldProps = {
              valueType: typeEnum,
            };
            propsMap[fieldId] = props;
            break;
          }
        }
      }
      setFieldInputProps(propsMap);
    }
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    loadFieldInputProps();
  }, [axiosContext, props.values.TemplateId, templateFieldIds]);

  return (
    <View style={styles.form}>
      <Dropdown
        data={templateDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.TemplateId}
        onChange={function (item: any): void {
          props.setFieldValue("TemplateId", +item.value);
        }}
        placeholder={"Pick a Template"}
      />

      <Dropdown
        data={transactorDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.SourceTransactorId}
        onChange={function (item: any): void {
          props.setFieldValue("SourceTransactorId", +item.value);
        }}
        placeholder={"Pick a Source"}
      />

      <Dropdown
        data={transactorDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.RecipientTransactorId}
        onChange={function (item: any): void {
          props.setFieldValue("RecipientTransactorId", +item.value);
        }}
        placeholder={"Pick a Recipient"}
      />

      <MultiSelect
        data={tagDropdownItems ?? []}
        labelField={"label"}
        valueField={"value"}
        value={props.values.TagIds}
        onChange={function (items: number[]): void {
          props.setFieldValue("TagIds", items);
        }}
        placeholder={"Pick Tags"}
      />

      {fieldInputProps !== undefined ? (
        templateFieldIds[props.values.TemplateId].map((fieldId) => (
          <FieldInput
            key={fieldId}
            valueType={fieldInputProps[fieldId].valueType}
            onChangeValue={function (
              enteredValue: string | boolean | Date
            ): void {
              const newFieldValues = props.values.fieldValues;
              newFieldValues[fieldId] = {
                value: enteredValue,
                fieldDatumId:
                  props.values.fieldValues[fieldId]?.fieldDatumId ?? NaN,
              };
              props.setFieldValue("fieldValues", newFieldValues);
            }}
            value={props.values.fieldValues[fieldId]?.value ?? ""}
          />
        ))
      ) : (
        <></>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 24,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default TransactionDetailScreen;
