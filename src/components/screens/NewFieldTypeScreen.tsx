import * as React from "react";
import * as Yup from "yup";
import { ErrorMessage, FormikProps } from "formik";
import { FieldTypeInfo, FieldTypeTypeEnum } from "../../models/FieldType";
import { StyleSheet, View } from "react-native";
import { Text, TextInput } from "react-native-paper";
import { Dropdown } from "react-native-element-dropdown";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { useEffect } from "react";

interface NewFieldTypeProps
  extends FormikProps<FieldTypeInfo>,
    FormikSetupProps {}

function FieldTypeDetailScreen(props: NewFieldTypeProps): JSX.Element {
  const setValidationSchema = props.setValidationSchema;

  useEffect(() => {
    if (setValidationSchema !== undefined) {
      const validationSchema = Yup.object().shape({
        name: Yup.string().required("Name is required."),
      });
      setValidationSchema(validationSchema);
    }
  }, [setValidationSchema]);

  return (
    <View style={styles.form}>
      <TextInput
        style={styles.input}
        placeholder="FieldType Name"
        autoCapitalize="words"
        onChangeText={props.handleChange("name")}
        onBlur={props.handleBlur("name")}
        value={props.values.name}
      />
      <ErrorMessage
        name="name"
        render={(msg) => <Text style={styles.input}>{msg}</Text>}
      />

      <Dropdown
        data={Object.keys(FieldTypeTypeEnum).map((e) => {
          return { label: e, value: e };
        })}
        labelField={"label"}
        valueField={"value"}
        value={props.values.type}
        onChange={function (item: any): void {
          props.setFieldValue("type", item.value);
        }}
        placeholder={"Pick a Type"}
      />

      <TextInput
        style={styles.input}
        placeholder="FieldType Validator"
        onChangeText={props.handleChange("validator")}
        onBlur={props.handleBlur("validator")}
        value={props.values.validator}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 24,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default FieldTypeDetailScreen;
