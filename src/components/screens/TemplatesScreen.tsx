import * as React from "react";
import { Pressable, StyleSheet, View } from "react-native";
import {
  TemplateBase,
  TemplateBaseInfo,
  TemplateTypeEnum,
} from "../../models/Template";
import {
  createTemplate,
  getTemplates,
  updateTemplate,
} from "../../blocs/TemplateBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewTemplateScreen from "./NewTemplateScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";

function TemplatesScreen({
  navigation,
}: HomeDrawerScreenProps<"Templates">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [accountTemplates, setAccountTemplates] = useState<TemplateBase[]>();
  const [entityTemplates, setEntityTemplates] = useState<TemplateBase[]>();
  const [transactionTemplates, setTransactionTemplates] =
    useState<TemplateBase[]>();
  const [isVisibleTemplateInputDialog, setIsVisibleTemplateInputDialog] =
    useState(false);
  const [objectValues, setObjectValues] = useState<TemplateBaseInfo>({
    name: "",
    isTemplate: true,
    type: TemplateTypeEnum.Account,
    TagIds: [],
    FieldIds: [],
  });
  const [requestedTemplate, setRequestedTemplate] =
    useState<TemplateBase | null>(null);
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Template");

  useEffect(() => {
    async function loadAccountTemplates(): Promise<void> {
      const accountTemplatesInfo = await getTemplates(
        axiosContext,
        TemplateTypeEnum.Account
      );
      setAccountTemplates(accountTemplatesInfo.flat());
    }

    if (!isVisibleTemplateInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadAccountTemplates();
    }
  }, [axiosContext, isVisibleTemplateInputDialog]);

  useEffect(() => {
    async function loadEntityTemplates(): Promise<void> {
      const entityTemplatesInfo = await getTemplates(
        axiosContext,
        TemplateTypeEnum.Entity
      );
      setEntityTemplates(entityTemplatesInfo.flat());
    }

    if (!isVisibleTemplateInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadEntityTemplates();
    }
  }, [axiosContext, isVisibleTemplateInputDialog]);

  useEffect(() => {
    async function loadTransactionTemplates(): Promise<void> {
      const transactionTemplatesInfo = await getTemplates(
        axiosContext,
        TemplateTypeEnum.Transaction
      );
      setTransactionTemplates(transactionTemplatesInfo.flat());
    }

    if (!isVisibleTemplateInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadTransactionTemplates();
    }
  }, [axiosContext, isVisibleTemplateInputDialog]);

  useEffect(() => {
    const objectValues: TemplateBaseInfo = {
      name: requestedTemplate?.name ?? "",
      isTemplate: requestedTemplate?.isTemplate ?? true,
      type: requestedTemplate?.type ?? TemplateTypeEnum.Account,
      TagIds: requestedTemplate?.TagIds ?? [],
      FieldIds: requestedTemplate?.FieldIds ?? [],
    };
    setObjectValues(objectValues);
  }, [requestedTemplate]);

  function showTemplateInputDialog(template: TemplateBase): void {
    setRequestedTemplate(template);
    setPrimaryButton("Update");
    setTitle(template.name);
    setIsVisibleTemplateInputDialog(true);
  }

  function showNewTemplateInputDialog(): void {
    setRequestedTemplate(null);
    setPrimaryButton("Create");
    setTitle("New Template");
    setIsVisibleTemplateInputDialog(true);
  }

  function hideTemplateInputDialog(): void {
    setIsVisibleTemplateInputDialog(false);
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewTemplateInputDialog),
    });
  });

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleTemplateInputDialog}
        toggleVisibilty={hideTemplateInputDialog}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: TemplateBaseInfo) => {
          if (primaryButton === "Create") {
            await createTemplate(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateTemplate(
              axiosContext,
              requestedTemplate?.id ?? NaN,
              values
            );
          }
        }}
        InputScreen={NewTemplateScreen}
        title={title}
        objectValues={{ ...objectValues }}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={accountTemplates}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showTemplateInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={entityTemplates}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showTemplateInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={transactionTemplates}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showTemplateInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default TemplatesScreen;
