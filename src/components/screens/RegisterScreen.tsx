import * as React from "react";
import * as Yup from "yup";
import { Button, Text, TextInput } from "react-native-paper";
import { ErrorMessage, Formik } from "formik";
import { ScrollView, StyleSheet, View } from "react-native";
import { useContext, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import MessageModal from "../modals/MessageModal";
import { RootStackScreenProps } from "../../types/NavigationTypes";
import SpinnerModal from "../modals/SpinnerModal";

interface RegisterValues {
  email: string;
  username: string;
  password: string;
}

function RegisterScreen({
  navigation,
}: RootStackScreenProps<"Register">): JSX.Element {
  const { publicAxios } = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [message, setMessage] = useState("");
  const [showMessage, setShowMessage] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);

  const initialValues: RegisterValues = {
    email: "",
    username: "",
    password: "",
  };

  const registerSchema = Yup.object().shape({
    email: Yup.string().required("Email is required."),
    username: Yup.string().required("Username is required."),
    password: Yup.string().required("Password is reqiured."),
  });

  async function onRegister(values: RegisterValues): Promise<void> {
    try {
      screenStatus.current = "awaiting";
      const response = await publicAxios.post("/auth/signup", {
        email: values.email,
        username: values.username,
        password: values.password,
      });
      setMessage(response.data.message as string);
      setIsRegistered(true);
    } catch (error: any) {
      setMessage(error.response.data.message as string);
    } finally {
      screenStatus.current = "idle";
      setShowMessage(true);
    }
  }

  function closeMessage(): void {
    setShowMessage(false);
    if (isRegistered) {
      navigation.navigate("Login");
    }
  }

  return (
    <>
      <MessageModal
        isVisible={showMessage}
        message={message}
        callback={closeMessage}
      />
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <ScrollView contentContainerStyle={styles.scrollView}>
        <Text style={styles.logo}>Flexible Finance</Text>
        <Formik
          initialValues={initialValues}
          validationSchema={registerSchema}
          onSubmit={async (values: RegisterValues) => await onRegister(values)}
        >
          {({ handleChange, handleBlur, handleSubmit, isValid, values }) => (
            <View style={styles.form}>
              <TextInput
                style={styles.input}
                placeholder="Email"
                keyboardType="email-address"
                autoCapitalize="none"
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                value={values.email}
              />
              <ErrorMessage
                name="email"
                render={(msg) => <Text style={styles.input}>{msg}</Text>}
              />

              <TextInput
                style={styles.input}
                placeholder="Username"
                keyboardType="email-address"
                autoCapitalize="none"
                onChangeText={handleChange("username")}
                onBlur={handleBlur("username")}
                value={values.username}
              />
              <ErrorMessage
                name="username"
                render={(msg) => <Text style={styles.input}>{msg}</Text>}
              />

              <TextInput
                style={styles.input}
                placeholder="Password"
                //              placeholderTextColor="#fefefe"
                secureTextEntry
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                value={values.password}
              />
              <ErrorMessage
                name="password"
                render={(msg) => <Text style={styles.input}>{msg}</Text>}
              />

              <Button
                disabled={!isValid || screenStatus.current !== "idle"}
                onPress={handleSubmit}
              >
                Register
              </Button>
            </View>
          )}
        </Formik>
        <Button
          onPress={() => {
            navigation.navigate("Login");
          }}
        >
          or Login
        </Button>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  logo: {
    fontSize: 60,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default RegisterScreen;
