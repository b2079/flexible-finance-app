import * as React from "react";
import { Pressable, StyleSheet, View } from "react-native";
import { Tag, TagInfo } from "../../models/Tag";
import { createTag, getTags, updateTag } from "../../blocs/TagBloc";
import { useContext, useEffect, useRef, useState } from "react";
import { AxiosContext } from "../../contexts/AxiosContext";
import { FlatList } from "react-native-gesture-handler";
import { HomeDrawerScreenProps } from "../../types/NavigationTypes";
import InputDialog from "../modals/InputDialog";
import NewTagScreen from "./NewTagScreen";
import PlusButton from "../buttons/PlusButton";
import SpinnerModal from "../modals/SpinnerModal";
import { Text } from "react-native-paper";

function TagsScreen({
  navigation,
}: HomeDrawerScreenProps<"Tags">): JSX.Element {
  const axiosContext = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [tags, setTags] = useState<Tag[]>();
  const [isVisibleTagInputDialog, setIsVisibleTagInputDialog] = useState(false);
  const [objectValues, setObjectValues] = useState<TagInfo>({
    name: "",
  });
  const [requestedTag, setRequestedTag] = useState<Tag | null>(null);
  const [primaryButton, setPrimaryButton] = useState("Create");
  const [title, setTitle] = useState("New Tag");

  useEffect(() => {
    const objectValues: TagInfo = {
      name: requestedTag?.name ?? "",
    };
    setObjectValues(objectValues);
  }, [requestedTag]);

  function showTagInputDialog(tag: Tag): void {
    setRequestedTag(tag);
    setPrimaryButton("Update");
    setTitle(tag.name);
    setIsVisibleTagInputDialog(true);
  }

  function showNewTagInputDialog(): void {
    setRequestedTag(null);
    setPrimaryButton("Create");
    setTitle("New Tag");
    setIsVisibleTagInputDialog(true);
  }

  function hideTagInputDialog(): void {
    setIsVisibleTagInputDialog(false);
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => PlusButton(showNewTagInputDialog),
    });
  });

  useEffect(() => {
    async function loadTags(): Promise<void> {
      try {
        screenStatus.current = "awaiting";
        const tagsInfo = await getTags(axiosContext);
        screenStatus.current = "idle";
        setTags(tagsInfo);
      } catch {
        screenStatus.current = "idle";
      }
    }

    if (!isVisibleTagInputDialog) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      loadTags();
    }

    return function cleanup() {
      screenStatus.current = "idle";
    };
  }, [axiosContext, isVisibleTagInputDialog]);

  return (
    <>
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />
      <InputDialog
        dismissable={true}
        visible={isVisibleTagInputDialog}
        toggleVisibilty={hideTagInputDialog}
        primaryButtonLabel={primaryButton}
        onSubmit={async (values: TagInfo) => {
          if (primaryButton === "Create") {
            await createTag(axiosContext, values);
          } else if (primaryButton === "Update") {
            await updateTag(axiosContext, requestedTag?.id ?? NaN, values);
          }
        }}
        InputScreen={NewTagScreen}
        title={title}
        objectValues={objectValues}
      />
      <FlatList
        contentContainerStyle={styles.scrollView}
        data={tags}
        renderItem={({ item }) => (
          <View>
            <Pressable onPress={() => showTagInputDialog(item)}>
              <Text>{item.name}</Text>
            </Pressable>
          </View>
        )}
      ></FlatList>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default TagsScreen;
