import * as React from "react";
import * as Yup from "yup";
import { Button, Text, TextInput } from "react-native-paper";
import { ErrorMessage, Formik } from "formik";
import { ScrollView, StyleSheet, View } from "react-native";
import { useContext, useRef, useState } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { AxiosContext } from "../../contexts/AxiosContext";
import MessageModal from "../modals/MessageModal";
import { RootStackScreenProps } from "../../types/NavigationTypes";
import SpinnerModal from "../modals/SpinnerModal";

interface LoginValues {
  username: string;
  password: string;
}

function LoginScreen({
  navigation,
}: RootStackScreenProps<"Login">): JSX.Element {
  const authContext = useContext(AuthContext);
  const { publicAxios } = useContext(AxiosContext);
  const screenStatus = useRef("idle");
  const [message, setMessage] = useState("");
  const [showMessage, setShowMessage] = useState(false);

  const initialValues: LoginValues = {
    username: "",
    password: "",
  };

  const loginSchema = Yup.object().shape({
    username: Yup.string().required("Username is required."),
    password: Yup.string().required("Password is reqiured."),
  });

  async function onLogin(values: LoginValues): Promise<void> {
    try {
      screenStatus.current = "awaiting";
      const response = await publicAxios.post("/auth/signin", {
        username: values.username,
        password: values.password,
      });
      const { accessToken, refreshToken, id } = response.data;
      await authContext.login(
        accessToken as string,
        refreshToken as string,
        id as number
      );
    } catch (error: any) {
      console.log(error.message);
      screenStatus.current = "idle";
      setMessage(error.message as string);
      setShowMessage(true);
    }
  }

  return (
    <>
      <MessageModal
        isVisible={showMessage}
        message={message}
        callback={() => setShowMessage(false)}
      />
      <SpinnerModal isVisible={screenStatus.current === "awaiting"} />

      <ScrollView contentContainerStyle={styles.scrollView}>
        <Text style={styles.logo}>Flexible Finance</Text>
        <Formik
          initialValues={initialValues}
          validationSchema={loginSchema}
          onSubmit={async (values: LoginValues) => await onLogin(values)}
        >
          {({ handleChange, handleBlur, handleSubmit, isValid, values }) => (
            <View style={styles.form}>
              <TextInput
                style={styles.input}
                placeholder="Username"
                autoCapitalize="none"
                onChangeText={handleChange("username")}
                onBlur={handleBlur("username")}
                value={values.username}
              />
              <ErrorMessage
                name="username"
                render={(msg) => <Text style={styles.input}>{msg}</Text>}
              />

              <TextInput
                style={styles.input}
                placeholder="Password"
                secureTextEntry
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                value={values.password}
              />
              <ErrorMessage
                name="password"
                render={(msg) => <Text style={styles.input}>{msg}</Text>}
              />

              <Button
                disabled={!isValid || screenStatus.current !== "idle"}
                onPress={handleSubmit}
              >
                Sign In
              </Button>
            </View>
          )}
        </Formik>
        <Button
          onPress={() => {
            navigation.navigate("Register");
          }}
        >
          or Register
        </Button>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  scrollView: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  logo: {
    fontSize: 60,
    margin: "2%",
  },
  form: {
    width: "80%",
    maxWidth: 800,
    margin: "2%",
  },
  input: {
    fontSize: 20,
    paddingBottom: 10,
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
    marginVertical: 20,
  },
  button: {},
});

export default LoginScreen;
