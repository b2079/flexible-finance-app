import { IconButton } from "react-native-paper";
import React from "react";

function PlusButton(callback: VoidFunction): JSX.Element {
  return <IconButton icon={"plus"} onPress={() => callback()} />;
}

export default PlusButton;
