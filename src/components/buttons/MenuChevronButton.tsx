import { IconButton } from "react-native-paper";
import React from "react";

function MenuChevron(
  isMenuVisible: boolean,
  toggleMenu: VoidFunction
): JSX.Element {
  return (
    <IconButton
      icon={isMenuVisible ? "chevron-up" : "chevron-down"}
      onPress={() => toggleMenu()}
    />
  );
}

export default MenuChevron;
