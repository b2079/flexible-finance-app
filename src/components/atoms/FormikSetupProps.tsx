export default interface FormikSetupProps {
  setInitialValues?: Function;
  setValidationSchema?: Function;
}
