import { Switch, TextInput } from "react-native-paper";
import { SwitchProps, TextInputProps } from "react-native";
import { DatePickerInput } from "react-native-paper-dates";
import { DatePickerModalContentSingleProps } from "react-native-paper-dates/lib/typescript/Date/DatePickerModalContent";
import { FieldTypeTypeEnum } from "../../models/FieldType";
import React from "react";

interface DateFieldProps
  extends Omit<DatePickerModalContentSingleProps, "date" | "onChange">,
    Omit<TextInputProps, "value"> {
  onChangeValue?: (newDate: Date | string) => void;
  value?: Date | string | boolean;
  valueType: FieldTypeTypeEnum;
}

interface NumberFieldProps
  extends Omit<TextInputProps, "onChangeText" | "value"> {
  onChangeValue?: (text: string) => void;
  valueType: FieldTypeTypeEnum;
  value?: Date | string | boolean;
}

interface SwitchFieldProps
  extends Omit<SwitchProps, "onValueChange" | "value"> {
  onChangeValue?: (value: boolean) => void;
  valueType: FieldTypeTypeEnum;
  value?: Date | string | boolean;
}

interface TextFieldProps
  extends Omit<TextInputProps, "onChangeText" | "value"> {
  onChangeValue?: (text: string) => void;
  valueType: FieldTypeTypeEnum;
  value?: Date | string | boolean;
}

function FieldInput(
  props: DateFieldProps | NumberFieldProps | SwitchFieldProps | TextFieldProps
): JSX.Element {
  const valueType = props.valueType;

  if (valueType === FieldTypeTypeEnum.Text) {
    const textProps = props as TextFieldProps;
    return (
      <TextInput
        style={textProps.style}
        placeholder={textProps.placeholder}
        onChangeText={textProps.onChangeValue}
        onBlur={textProps.onBlur}
        value={textProps.value as string}
        editable={textProps.editable}
        multiline={textProps.multiline}
      />
    );
  } else if (valueType === FieldTypeTypeEnum.Number) {
    const numberProps = props as NumberFieldProps;
    return (
      <TextInput
        style={numberProps.style}
        placeholder={numberProps.placeholder}
        onChangeText={numberProps.onChangeValue}
        onBlur={numberProps.onBlur}
        value={numberProps.value as string}
        editable={numberProps.editable}
        keyboardType="numeric"
      />
    );
  } else if (valueType === FieldTypeTypeEnum.Date) {
    const dateProps = props as DateFieldProps;
    return (
      <DatePickerInput
        locale={dateProps.locale}
        value={dateProps.value as Date}
        onChange={(d) => {
          if (dateProps?.onChangeValue !== undefined)
            dateProps?.onChangeValue(d as Date);
        }}
        inputMode="start"
      />
    );
  } else if (valueType === FieldTypeTypeEnum.Boolean) {
    const boolProps = props as SwitchFieldProps;
    return (
      <Switch
        onValueChange={boolProps.onChangeValue}
        value={boolProps.value as boolean}
      />
    );
  }
  return <div></div>;
}

export default FieldInput;

export { DateFieldProps, NumberFieldProps, SwitchFieldProps, TextFieldProps };
