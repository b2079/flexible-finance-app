import * as Yup from "yup";
import { Button, Dialog, Portal } from "react-native-paper";
import { KeyboardAvoidingView, Platform } from "react-native";
import React, { useState } from "react";
import { Formik } from "formik";
import FormikSetupProps from "../atoms/FormikSetupProps";
import { ScrollView } from "react-native-gesture-handler";

interface InputDialogProps extends FormikSetupProps {
  dismissable?: boolean;
  onDismiss?: () => void;
  overlayAccessibilityLabel?: string;
  visible: boolean;
  toggleVisibilty: VoidFunction;
  title: string;
  cancelButtonLabel?: string;
  primaryButtonLabel: string;
  onSubmit: Function;
  InputScreen: Function;
  objectValues: object;
}

function InputDialog({
  dismissable,
  onDismiss,
  overlayAccessibilityLabel,
  visible,
  toggleVisibilty,
  title,
  cancelButtonLabel,
  primaryButtonLabel,
  onSubmit,
  InputScreen,
  objectValues,
}: InputDialogProps): JSX.Element {
  const [validationSchema, setValidationSchema] =
    useState<Yup.AnyObjectSchema>();

  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss ?? toggleVisibilty}>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
        >
          <Dialog.Title>{title}</Dialog.Title>
          <Dialog.ScrollArea>
            <ScrollView>
              <Formik
                enableReinitialize
                initialValues={objectValues}
                validationSchema={validationSchema}
                onSubmit={async (values: any) => {
                  await onSubmit(values);
                  toggleVisibilty();
                }}
              >
                {(formikProps) => (
                  <>
                    <InputScreen
                      setValidationSchema={setValidationSchema}
                      {...formikProps}
                      {...objectValues}
                    />

                    <Dialog.Actions>
                      <Button onPress={toggleVisibilty}>
                        {cancelButtonLabel ?? "Cancel"}
                      </Button>
                      <Button onPress={formikProps.handleSubmit}>
                        {primaryButtonLabel}
                      </Button>
                    </Dialog.Actions>
                  </>
                )}
              </Formik>
            </ScrollView>
          </Dialog.ScrollArea>
        </KeyboardAvoidingView>
      </Dialog>
    </Portal>
  );
}

export default InputDialog;
