import { ActivityIndicator, Modal, Portal } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import React from "react";

interface SpinnerProps {
  isVisible: boolean;
}

function SpinnerModal(props: SpinnerProps): JSX.Element {
  return (
    <Portal>
      <Modal visible={props.isVisible} dismissable={false}>
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#007aff" />
        </View>
      </Modal>
    </Portal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default SpinnerModal;
