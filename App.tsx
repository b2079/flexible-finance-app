/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/consistent-type-definitions */
import "react-native-gesture-handler";
import {
  NavigationContainer,
  DarkTheme as NavigationDarkTheme,
  DefaultTheme as NavigationDefaultTheme,
} from "@react-navigation/native";
import {
  DarkTheme as PaperDarkTheme,
  DefaultTheme as PaperDefaultTheme,
  Provider as PaperProvider,
} from "react-native-paper";
import React, { useContext, useRef, useState } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import axios, { AxiosRequestConfig } from "axios";
import {
  // en,
  // nl,
  // de,
  // pl,
  // pt,
  enGB,
  registerTranslation,
} from "react-native-paper-dates";
import { AuthContext } from "./src/contexts/AuthContext";
import { AxiosContext } from "./src/contexts/AxiosContext";
import HomeScreen from "./src/components/screens/HomeScreen";
import LoginScreen from "./src/components/screens/LoginScreen";
import RegisterScreen from "./src/components/screens/RegisterScreen";
import { RootStackParamList } from "./src/types/NavigationTypes";
import SpinnerModal from "./src/components/modals/SpinnerModal";
import createAuthRefreshInterceptor from "axios-auth-refresh";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

// registerTranslation('en', en)
// registerTranslation('nl', nl)
// registerTranslation('pl', pl)
// registerTranslation('pt', pt)
// registerTranslation('de', de)
registerTranslation("en-GB", enGB);

function Root(): JSX.Element {
  const defaultAuthContext = useContext(AuthContext);
  const defaultAxiosContext = useContext(AxiosContext);
  const [authState, setAuthState] = useState(defaultAuthContext.authState);
  const isSpinnerVisible = useRef(false);
  function toggleSpinner(value?: boolean): void {
    if (value === undefined) {
      isSpinnerVisible.current = !isSpinnerVisible.current;
    } else {
      isSpinnerVisible.current = value;
    }
  }

  const authContext = {
    authState,
    setAuthState,
    getAuthInfo: defaultAuthContext.getAuthInfo,
    setAuthInfo: defaultAuthContext.setAuthInfo,
    deleteAuthInfo: defaultAuthContext.deleteAuthInfo,
    getAccessToken: defaultAuthContext.getAccessToken,
    login,
    logout,
    refresh,
  };

  const axiosContext = {
    apiUrl: defaultAxiosContext.apiUrl,
    publicAxios: defaultAxiosContext.publicAxios,
    privateAxios: defaultAxiosContext.privateAxios,
    isSpinnerVisible,
    toggleSpinner,
  };

  async function login(
    accessToken: string,
    refreshToken: string,
    userId: number
  ): Promise<void> {
    await authContext.setAuthInfo(accessToken, refreshToken, userId);
    authContext.setAuthState({
      refreshToken,
      authenticated: true,
      userId,
    });
    return await Promise.resolve();
  }

  async function logout(): Promise<void> {
    authContext.setAuthState({
      refreshToken: "",
      authenticated: false,
      userId: NaN,
    });
    await authContext.deleteAuthInfo();
    return await Promise.resolve();
  }

  async function refresh(): Promise<void> {
    try {
      const jsonAuthInfo = await authContext.getAuthInfo();
      if (jsonAuthInfo !== "") {
        const authInfo = JSON.parse(jsonAuthInfo);
        if (
          authInfo.accessToken === "" ||
          //        authInfo.refreshToken === "" ||
          isNaN(authInfo.userId)
        ) {
          return await authContext.logout();
        }
        authContext.setAuthState({
          refreshToken: authInfo.refreshToken,
          authenticated: true,
          userId: authInfo.userId,
        });
      }
    } catch (error) {
      return await authContext.logout();
    }
  }

  axiosContext.privateAxios.interceptors.request.use(
    async (config) => {
      config.headers = {
        Authorization: `Bearer ${await authContext.getAccessToken()}`,
      };
      return config;
    },
    async (error) => {
      return await Promise.reject(error);
    }
  );

  async function refreshAuthLogic(failedRequest: any): Promise<void> {
    const data = {
      refreshToken: authState.refreshToken,
    };

    const options: AxiosRequestConfig = {
      method: "POST",
      data,
      url: axiosContext.apiUrl + "/refreshToken",
    };

    try {
      const tokenRefreshResponse = await axios(options);
      failedRequest.response.config.headers = {
        Authorization: `Bearer ${tokenRefreshResponse.data.accessToken}`,
      };

      await authContext.login(
        tokenRefreshResponse.data.accessToken,
        authState.refreshToken,
        authState.userId
      );
    } catch (error) {
      await authContext.logout();
    }
  }

  createAuthRefreshInterceptor(axiosContext.privateAxios, refreshAuthLogic, {});
  const StackNav = createNativeStackNavigator<RootStackParamList>();

  return (
    <AuthContext.Provider value={{ ...authContext }}>
      <AxiosContext.Provider value={{ ...axiosContext }}>
        <PaperProvider theme={CombinedDefaultTheme}>
          <SafeAreaView style={styles.safeAreaView}>
            <SpinnerModal isVisible={axiosContext.isSpinnerVisible.current} />
            <NavigationContainer theme={CombinedDefaultTheme}>
              <StackNav.Navigator>
                {authContext.authState.authenticated ? (
                  <StackNav.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{ headerShown: false }}
                  />
                ) : (
                  <>
                    <StackNav.Screen
                      name="Login"
                      component={LoginScreen}
                      options={{ headerShown: false }}
                    />
                    <StackNav.Screen
                      name="Register"
                      component={RegisterScreen}
                      options={{ headerShown: false }}
                    />
                  </>
                )}
              </StackNav.Navigator>
            </NavigationContainer>
          </SafeAreaView>
        </PaperProvider>
      </AxiosContext.Provider>
    </AuthContext.Provider>
  );
}

const CombinedDefaultTheme = {
  ...PaperDefaultTheme,
  ...NavigationDefaultTheme,
  colors: {
    ...PaperDefaultTheme.colors,
    ...NavigationDefaultTheme.colors,
  },
};
const CombinedDarkTheme = {
  ...PaperDarkTheme,
  ...NavigationDarkTheme,
  colors: {
    ...PaperDarkTheme.colors,
    ...NavigationDarkTheme.colors,
  },
};

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
});

export default Root;
