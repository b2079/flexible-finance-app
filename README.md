# Flexible Finance App

The front-end to [Flexible Finance Server](https://gitlab.com/b2079/flexible-finance-server). Together, they form Flexible Finance, a money management app that aims to support the casual user by providing functionality and flexibility without the intensity of serious accounting apps.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) installed.

Make sure you have [Expo](https://docs.expo.dev/get-started/installation/) installed.

_Get the code:_

```sh
$ git clone https://gitlab.com/b2079/flexible-finance-app.git # or clone your own fork
$ cd flexible-finance-app
```

_Set up your environment:_

```sh
$ npm install
$ cp .env.example .env
# Open .env and replace the values in <angle brackets> with your own
```

_Build and run the app:_

```sh
$ npm run build
$ npx serve web-build
```

Your app should now be running on [localhost:3000](http://localhost:3000/).

## Development

There is support in the configuration files for automatic linting with Visual Studio Code, with the ESLint and Prettier extensions installed.

## Deploying to Heroku

There is a continuous deployment pipeline between the GitLab repository and the Heroku app. When a commit is pushed to `main`, GitLab will deploy that code to Heroku.

The server app can be viewed at https://flexible-finance-server.herokuapp.com/. The client app can be viewed at https://flexible-finance-app.herokuapp.com/.
